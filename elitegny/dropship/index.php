<?php

/**
 * /***
##
##  INVOICE - 1 time a day.
##
*/

/**
 * 

810_daily_invoice_export_auto at 4:55PM every day;
=> C:\inetpub\elitegny\helz\810_daily_invoice_export_auto.bat
{
    SET CurrentDate=%date%
    cd C:\inetpub\elitegny\helz\
    cscript //nologo C:\inetpub\elitegny\helz\URLChecker.vbs "http://edi.elitegny.com/helz/810_daily_invoice_export_generate_once.php"
    ping 127.0.0.1
    for /f "tokens=*" %%a in (list_of_ordernos_invoicing.txt) do  cscript //nologo C:\inetpub\elitegny\helz\URLChecker.vbs "http://edi.elitegny.com/helz/810_daily_invoice_export_outbox2.php?listorderno=%%a" 
    ping 127.0.0.1
    for /f "tokens=*" %%a in (list_of_ordernos_invoicing.txt) do  xcopy "C:\inetpub\elitegny\helz\outbox_log\810_%%a_*.edi" "C:\Users\Public\FTP Folder\AS2 Data\Helzberg\outbox\" /D:%CurrentDate%

}
 */


/***
##
##  INVENTORY FEED -> 24 times a day 
##
***/

/**
846_helz_inventory_feed at 4:55PM every day - After triggered, repeat every 1 hour for a duration of 1 day.
=> C:\inetpub\elitegny\helz\846_daily_inventory_auto.bat
{
    SET CurrentDate=%date%
    cd C:\inetpub\elitegny\helz\
    cscript //nologo C:\inetpub\elitegny\helz\URLChecker.vbs "http://edi.elitegny.com/helz/846_daily_inventory_export_outbox2.php"

    ## recursive end when output < 1
    ping 127.0.0.1
    ping 127.0.0.1
    ping 127.0.0.1
    xcopy "C:\inetpub\elitegny\helz\outbox_log\846_*.edi" "C:\Users\Public\FTP Folder\AS2 Data\Helzberg\Outbox\" /D:%CurrentDate%


}

*/

/***
##
##  ORDER CHECK : IN THE MORNI(NG -> 1 time
##
***/
/*
850_997_all_helz_inbox_checker At 9:20 AM every day
=> C:\inetpub\elitegny\edi_helz_inbox_checker.bat

{
    cscript //nologo C:\inetpub\elitegny\URLChecker.vbs "http://edi.elitegny.com/import_helz_edi.php"
    cd C:\Users\Public\FTP Folder\AS2 Data\Helzberg\Inbox
    for /r . %%g in (*.edi) do cscript //nologo C:\inetpub\elitegny\helz\URLChecker.vbs "http://edi.elitegny.com/helz/edi_inbox_checker.php?filename=%%~nxg"
    cd C:\inetpub\elitegny

}
 */
 

/***
##
##  ORDER CHECK  : 6AM - 6PM 
##
***/

/**
Helz_EDI_Receiving_Inbox_Checker At 6:00 AM every day - after triggered, repeat every 1 hour for a duration of 12 hours;
-> c:\inetpub\elitegny\edi_helz_inbox_checker.bat

{
    cscript //nologo C:\inetpub\elitegny\URLChecker.vbs "http://edi.elitegny.com/import_helz_edi.php"
    cd C:\Users\Public\FTP Folder\AS2 Data\Helzberg\Inbox
    for /r . %%g in (*.edi) do cscript //nologo C:\inetpub\elitegny\helz\URLChecker.vbs "http://edi.elitegny.com/helz/edi_inbox_checker.php?filename=%%~nxg"
    cd C:\inetpub\elitegny

}
*/
### 1. edi_helz_inbox.bat
### 2. dir_helz_inbox.bat (EDI filelist in FTP folder)
### 3. import to DB
### 4. /helz/edi_inbox_checker => read EDI :: check document type => move to web folder and save SalesOrder 


/***
##
##  SHIPPING NOTICE -> 1 time
##
***/

/**
856_daily_shipnotice_export_auto : At 4:55 PM every day
=> C:\inetpub\elitegny\helz\856_daily_shipnotice_export_auto.bat
{
    SET CurrentDate=%date%
    cd C:\inetpub\elitegny\helz\
    cscript //nologo C:\inetpub\elitegny\helz\URLChecker.vbs "http://edi.elitegny.com/helz/856_daily_shipnotice_export_outbox.php"
    ping 127.0.0.1
    ping 127.0.0.1
    ping 127.0.0.1
    xcopy "C:\inetpub\elitegny\helz\outbox_log\856_*.edi" "C:\Users\Public\FTP Folder\AS2 Data\Helzberg\outbox\" /D:%CurrentDate%
}
*/

/*
Generate UPS ShipList File at 12:01 AM - after triggered, repeat every 09:00:00 for a duration of 1 day
=> C:\inetpub\elitegny\helz\update_batch_trackno_file_generator.bat
*/



/*
SQL Backup    At 9:53 PM every Monday, Friday of every week, starting 9/27/12
C:\User\Administrator\Documents\Backup.bat
*/


echo phpinfo();

?>