CREATE DEFINER=`root`@`%` PROCEDURE `updateCheckExistSelectedEDI_new`(
	In v_edi_id Int(11)
    )
BEGIN
	DECLARE v_edi_msg,  v_split1,  v_split2 text;
	declare v_fcate varchar(20);
	Declare v_orderno  varchar(35);
	Declare v_orderid  varchar(35);
	DECLARE v_invno_result  CHAR(2);
	DECLARE v_invno  VARCHAR(15);
	
	declare v_memo TEXT;
	declare v_cust_no TINYINT (4);
	DECLARE v_cust_name varchar(20);
	declare v_tedcode varchar(5);
	DECLARE v_msg VARCHAR(255);	
		
		
	SET v_cust_no = (SELECT cust_no FROM TBL_EDI_List WHERE id = v_edi_id );		
	SET v_edi_msg = (SELECT fcontent FROM TBL_EDI_List WHERE id = v_edi_id );	
	-- select v_edi_msg;
	
	SET v_split1 = ( SELECT strSplit( v_edi_msg, "ST*", 2 ) ); 
	SET v_fcate = ( SELECT strSplit( v_split1, "*", 1 ) ); 
	IF v_cust_no = 5 THEN  -- HELZ	
		set v_cust_name = "HELZBERG";		
		set v_orderno = ( SELECT strSplit( v_split1, "*", 5 ) ); 		
				
				
	
		IF v_fcate = "824" THEN 
		
			SET v_split2 = ( SELECT strSplit( v_edi_msg, "~TED*", 2 ) ); 	
			SET v_tedcode =  ( SELECT strSplit( v_split2, "*", 1 ) ); 		
			SET v_msg =  ( SELECT strSplit( v_split2, "*", 2 ) ); 	
		
			SET v_split1 = ( SELECT strSplit( v_edi_msg, "*VN*", 2) ); 
			SET v_orderno = ( SELECT strSplit( v_split1, "~", 1) );  -- '123465'; 
			
			SET v_split1 = ( SELECT strSplit( v_edi_msg, "OTI*", 2 ) );			
			SET v_invno_result = ( SELECT strSplit( v_split1, "*", 1 ) );	-- TA : Accepted	
			SET v_invno = ( SELECT strSplit( v_split1, "*", 3 ) );	-- INV NO
			SET v_orderid  = ( SELECT strSplit( v_split1, "*", 9 ) ); -- orderid 						
						
			if v_invno_result = "TA" THEN
				SET v_memo = CONCAT('P.O Invoice - HZN', v_orderid,' has been accepted by ', v_cust_name);
			END IF ;
			IF v_invno_result = "TC" THEN
				SET v_memo = CONCAT('P.O Invoice - HZN', v_orderid,' ( w/ data change ) has been accepted by ', v_cust_name);
			END IF ;
			IF v_invno_result = "TR" THEN
				SET v_memo = CONCAT('P.O Invoice - HZN', v_orderid,' has been rejected by ', v_cust_name);				
			END IF ;	
			
			
			set v_memo = concat(v_memo, ' [ MSG:', v_invno_result, '_',v_tedcode,' =>', v_msg,']');
			
		END IF ;
		
	END IF;
	
	IF v_cust_no = 6 THEN  -- AMAZON
		SET v_cust_name = "AMAZON MYIA";						
		SET v_orderno = ( SELECT strSplit( v_split1, "*", 5 ) ); 	
	END IF;
	
	IF v_fcate = "850" THEN 
		SET v_memo = CONCAT('New P.O Received By ', v_cust_name);		
		
	END IF ;	
	IF v_fcate = "860" THEN 
		SET v_memo = CONCAT('P.O Information / Status Changed By ', v_cust_name);	
				
	END IF ;
	
	IF v_fcate = "997" THEN 
		SET v_split1 = ( SELECT strSplit( v_edi_msg, "AK2*", 2 ) ); 
		SET v_fcate = ( SELECT strSplit( v_split1, "*", 1 ) ); 
		IF v_fcate = "856" THEN
			SET v_orderid  = ( SELECT strSplit( v_split1, "*", 2 ) ); 
			SET v_orderid  = ( SELECT strSplit( v_orderid , "~", 1 ) ); 
			
			IF v_cust_no = 5 THEN 		-- helz	
				set v_orderno = ( select orderno from TBL_SalesOrder_Ds where id = v_orderid );			
			end if ;
			
			SET v_fcate = "997_856";
			SET v_memo = CONCAT('997(856) - PO: ', v_orderno, ' Ship-Notice has been received by ', v_cust_name);			
		
		END IF ;		
		IF v_fcate = "810" THEN
			SET v_orderid  = ( SELECT strSplit( v_split1, "*", 2 ) ); 
			SET v_orderid  = ( SELECT strSplit( v_orderid , "~", 1 ) ); 
			
			IF v_cust_no = 5 THEN 		-- helz	
				SET v_orderno = ( SELECT f_sonum FROM TBL_SOItemList_Ds WHERE id = v_orderid );
			END IF ;
			
			SET v_fcate = "997_810";
			SET v_memo = CONCAT('997(810) - PO: ', v_orderno, ' Invoice receipt has been received by ', v_cust_name);			
		
			select v_cust_no;
			SELECT v_memo;
		END IF ;		
	END IF ;
	
/*
ISA*00*          *00*          *01*006965735      *01*LXGM           
*140218*1159*U*00403*000038981*0*P*|~GS*FA*LXGM*006965735*20140218*1159*39055*X*004030~ST*997*30970~AK1*IB*29258~AK2*810*32685~AK5*A~AK9*A*1*1*1~SE*6*30970~GE*1*39055~IEA*1*000038981~
$arr_orderinfo = explode ( "~", $arr_rows[1]);
ISA*00*          *00*          *01*006965735      
*01*LXGM           
*131127*1302*U*00403*000029033*0*P*|~GS*FA*00*LXGM*20131127*1302*29107*X*004030~
ST*997*23123~AK1*SH*28295~AK2*856*95448~AK5*A~AK9*A*1*1*1~SE*6*23123~GE*1*29107~IEA*1*000029033~
	
//importEdi997ToDB($path, $orderno, "997(856) - PO: $orderno Ship-Notice has been received by K2B.");
ISA*00*          *00*          *01*006965735      *01*LXGM           
*140214*0755*U*00403*000038911*0*P*|~
GS*PO*006965735*LXGM*20140214*0755*38985*X*004030~ST*850*7799~BEG*00*OS*4505833**20140213~REF*IA*9665~REF*IT*S00012000252222~REF*CO*12376308~REF*IL*348467501~PER*BD*Jan Steck*WP*8166271527*EM*jmsteck@helzberg.com~CSH*SC~SAC*A*A260****2*10~ITD*14**0**0**60*****Net 60 days~DTM*001*20140222~DTM*002*20140215~CTB*SR*Overnight Delivery~N9*ZZ*MTX~MTX*ZZZ*Supplier's acceptance of this Purchase Order obligates Supplier to abide by all of the terms, conditions and instructions set forth in this Purchase Order and the Master Supplier Agreement between Supplier and Buyer. Additional or substitute terms will not become part of this Purchase Order unless expressly accepted in writing by Buyer.  PURSUANT TO THE MASTER SUPPLIER AGREEMENT, THIS PURCHASE ORDER CONTAINS A BINDING ARBITRATION PROVISION WHICH MAY BE ENFORCED BY THE PARTIES.~N9*ZZ*MTX~MTX*INT*RingSize:7.0~N9*ZZ*MTX~MTX*INT*CSO#:12376308    Store:0010 - Distribution Center    Cust:John Shiba    Need By:~N1*BT*Helzberg*92*0010~N3*1825 Swift Ave~N4*N Kansas City*MO*64116~N1*ST*John Shiba*92*0010~N3*5215 Wright Ter~N4*Skokie*IL*60077~PO1**1*EA*27.91*WH*IN*1858004*VN*R2423401*IZ*7.00~CTP**RTL~PID*F*08***SS DVITA SQ CZ RING~N9*QS*MTX~MTX*ZZZ*W00120100013926~CTT*1~SE*32*7799~GE*1*38985~IEA*1*000038911~
*/	
/*		
	select  v_orderno;
*/
	start transaction;
	update  TBL_EDI_List	set 
		fcate = v_fcate,
		orderno = v_orderno,
		memo = v_memo
	where 
	id = v_edi_id;
	
	IF v_cust_no = 5 AND v_orderno <> "" AND v_fcate = '997_856' THEN
		UPDATE  TBL_SalesOrder_Ds SET ship_confirmed = 'Y' 
		WHERE departmentid = 5 AND STATUS = 4 AND 
		orderno IN ( v_orderno );
	
	END IF;
			
	
	IF v_cust_no = 5 AND v_orderno <> "" AND v_fcate = '997_810' THEN
		UPDATE  TBL_SalesOrder_Ds SET inv_confirmed = 'Y' 
		WHERE departmentid = 5 AND
		STATUS = 4 AND ( inv_confirmed IS NULL OR  inv_confirmed = '' OR inv_confirmed = 'N' )  AND 
		orderno IN ( v_orderno );
	
	END IF;
	
	IF v_cust_no = 5 AND v_orderno <> "" AND v_fcate = '824' THEN
		IF v_invno_result = "TA" THEN
			UPDATE  TBL_SalesOrder_Ds SET inv_finished = 'A' 
			WHERE departmentid = 5 AND
			STATUS = 4 AND  ( inv_finished <> 'A'  ) AND 
			orderno IN ( v_orderno );			
		END IF;
		
		IF v_invno_result = "TR" THEN
			UPDATE  TBL_SalesOrder_Ds SET inv_finished = 'R' 
			WHERE departmentid = 5 AND
			STATUS = 4 AND  ( inv_finished IS NULL OR  inv_finished = '' OR inv_finished = 'N' ) AND 
			orderno IN ( v_orderno );			
		END IF;
		
	END IF;	
		
	commit;
	
    END
