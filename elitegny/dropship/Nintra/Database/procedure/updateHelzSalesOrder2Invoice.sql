CREATE DEFINER=`root`@`%` PROCEDURE `updateHelzSalesOrder2Invoice`(
        IN order_itemid INT
    )
BEGIN
	DECLARE new_invno VARCHAR(20);
	START TRANSACTION;
	
	set new_invno = concat ( 'HZN', order_itemid);
	
	update 
	TBL_SalesOrder_Ds t1, TBL_SOItemList_Ds t2 set	
	t1.invno = new_invno, t2.f_invno = new_invno,
	t1.inv_requested = 'Y',
	t1.invno_reqdate = now()
	where t1.id = t2.f_orderid and t2.id in (order_itemid);	
	
	commit ;
	
END