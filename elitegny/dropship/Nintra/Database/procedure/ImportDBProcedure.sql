
CREATE DEFINER=`root`@`%` PROCEDURE `importEDItoDB`(
        IN  v_org_fname  VARCHAR(100),
        IN  v_fcontent   text,
        IN  v_cust_no  INT(11),
        IN  v_cust_str VARCHAR(20)
    )
BEGIN
Declare out_edi_id INT(11);
START TRANSACTION;
/*
	DECLARE i INT DEFAULT 1;
	DECLARE vsplit TEXT;	
	DECLARE exist_orderid INT;	
	DECLARE exist_orderstatus INT;	
	
-- Variables for Basic Order Table
	DECLARE orderno VARCHAR(20);
	DECLARE shipcustname VARCHAR(40);
	DECLARE shipaddr1 VARCHAR(100);
	DECLARE shipaddr2 VARCHAR(100);
	DECLARE shipaddr3 VARCHAR(100);
	DECLARE shipcity VARCHAR(30);
	DECLARE shipstate VARCHAR(5);	
	DECLARE shipzip VARCHAR(15);
	DECLARE shipcountry VARCHAR(10);
	DECLARE shippingtype VARCHAR(10);	
	DECLARE docno VARCHAR(30);
	DECLARE phonenum VARCHAR(20);	
-- Variables for Order Item
	DECLARE soid INT;
	DECLARE price DOUBLE; 
	DECLARE qty INT;
	DECLARE f_itemid INT;
	DECLARE f_itemcode VARCHAR(20);
	DECLARE size VARCHAR(5);
	DECLARE f_vendorsku VARCHAR(25);
	DECLARE f_depnum VARCHAR(5);
	DECLARE cust_str VARCHAR(10);
	DECLARE ext1 VARCHAR(5);
	DECLARE cust_id VARCHAR(10);
	
	SET vsplit = ( SELECT SPLIT_STR( list_orderitems, '||', 1) );	
	SET vsplit = ( SELECT REPLACE(vsplit, '"', '') );		
	SET orderno = ( SELECT SPLIT_STR( vsplit, ',', 1) );		
*/
--	insert ignore into TBL_EDI_List ( org_fname, fname, fcontent, regdate, cust_no, cust_name, status ) values 
	insert  ignore into TBL_EDI_List ( org_fname, fname, fcontent, regdate, cust_no, cust_name, status ) values 
		(v_org_fname, '', v_fcontent, now(),v_cust_no, v_cust_str, 1);
	SET out_edi_id = LAST_INSERT_ID();
	
	if out_edi_id <> '' then
	CALL updateCheckExistSelectedEDI_new ( out_edi_id );
	end if;
/*	
	insert ignore into TBL_EDI_List ( org_fname, fname, fcontent, regdate, cust_no, cust_name ) values 
		('$filename', '','$row_content',now(),6,'Amazon Myia');
*/		
		
	-- org_fname, fname, fcontent, regdate, cust_no, cust_name
COMMIT;
	
    END