<?php

namespace dropship\Nintra\Database;

class NintraDB {

    private $db; //db conncion 
    private $result; //result set
    private $sql;
    
    public function __construct($host = "nintra.elitegny.com",$username = "elitegny",$password = "elitegny",$db_name = "egnymasterdb", $db_handle = '') {

        $this->connect_db($host, $username, $password);
        $this->select_db($db_name);
    }

    public function connect_db($db_host, $db_user, $db_pass) {
        $this->db = @mysql_connect($db_host, $db_user, $db_pass);
        if (is_resource($this->db) == false) {
            $this->trace_error($db_host . " is not reachble.");
        }
    }

    public function select_db($db_name) {
        @mysql_select_db($db_name, $this->db) or $this->trace_error("failed to connect to ".$db_name);
    }

    public function execute_query($query) {

        if (is_resource($this->db) == false) {
            $this->trace_error("execute_query error. invalid db connection.");
            return;
        }
        $this->result = @mysql_query($query, $this->db) or trace_error("query not vaild.");
    }

    public function update($query) {
       
        $this->result = mysql_query($query, $this->db); 

    }

    public function get_result() {
        return $this->result;
    }

    public function query($query) {
        $this->result = mysql_query($query, $this->db);
        return $this->result;
    }

    public function get_query_data($query) {
        if (empty($query))
            $this->throw_error(__METHOD__, __LINE__, "Query missing.");

        $this->sql = $query;
        $this->result = $this->query($this->sql);
        if (!$this->result) {
            $this->throw_error(__METHOD__, __LINE__);
        }

        $ar_results = array();
        while ($obj = mysql_fetch_assoc($this->result)) {
            $ar_results[] = $obj;
        }

        if (is_array($ar_results)) {
            return $ar_results;
        } else {
            return false;
        }
    }
    
    public function last_inserted_id(){
        return mysql_insert_id();
    }

    public function execute_simple_query($query) {
        $result = mysql_query($query, $this->db);
        if (!is_resource($result))
            return -1;
        return mysql_result($result, 0, 0);
    }

    public function get_num_rows() {
        if (is_resource($this->result))
            return mysql_num_rows($this->result);
        return -1;
    }

    public function next_fetch() {
        return mysql_fetch_assoc($this->result);
    }

    public function next_row() {
        return mysql_fetch_assoc($this->result);
    }

    public function trace_error($msg) {
        echo "</UL></DL></OL>\n";
        echo "</TABLE></SCRIPT>\n";
        $text = "<FONT COLOR=\"#ff0000\"><P>Error: $msg : </p>";
        $text .= mysql_error();
        $text .= "</FONT>\n";
        die($text);
    }

    public function destroy() {
        if (is_resource($this->result)){
            mysql_free_result($this->result);
        }
        mysql_close($this->db);
    }

    public function throw_error($function_name, $line_number, $error_msg = '') {
        die('<font style="color: red; font-weight: bold;">-- Error --</font><br />' . $function_name . ' at line ' . $line_number . '<br />' . (!empty($error_msg) ? "<br />Message: $error_msg" : "Invalid Query: {$this->sql}"));
    }

    public function close() {
        $this->destroy();
    }

    public function begin() {
        mysql_query("BEGIN");
    }

    public function commit() {
        mysql_query("COMMIT");
    }

    public function rollback() {
        mysql_query("ROLLBACK");
    }

}

?>