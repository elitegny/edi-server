<?php

namespace dropship\HelzbergEDI;
include "Invoice/InvoiceList.php";
include "operation/EDIFile.php";

use dropship\HelzbergEDI\operation\EDIFile;
use dropship\HelzbergEDI\Invoice\InvoiceList;

$str = InvoiceList::generate();
EDIFile::writeInvoiceList($str);
