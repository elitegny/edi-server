<?php
namespace dropship\HelzbergEDI;
include("Configuration.php");
include_once("Inventory/HelzInventoryHandler.php");
include_once("operation/EDIFile.php");

use dropship\HelzbergEDI\Inventory\HelzInventoryHandler;
use dropship\HelzbergEDI\operation\EDIFile;

use dropship\Nintra\Database\NintraDB;



$sdate = $_GET['sdate']; 

if ( !isset($sdate) || $sdate == "" ){ 
	$sdate = date('Y-m-d', time()); 
}

$edistring = HelzInventoryHandler::export($sdate);
EDIFile::writeInventory($edistring);