<?php

/*
"GD"  for  "Ground Delivery"
"2D"  for  "2-Day Delivery"
"1D"  for  "Overnight Delivery"
"FGD"  for  "FREE Ground Delivery"
"F1D"  for  "FREE Next Day Delivery"
2-Day Delivery
Ground Delivery
FREE Ground Delivery
FREE Next Day Delivery
FREE 2-Day Delivery
Saturday Delivery
FREE Next-Day Delivery
MAIL
*/
namespace dropship\HelzbergEDI\PurchaseOrder;

class PoEdiParser{
    
    private $parsed = array();
    //850 EDI Documentn Parser
    const EDI_TYPE_NO = "850";
    
    function __construct() {
        ;
    }
    
    function getCollection(){
        return $this->parsed;
    }
    
    function addItem($key,$value){
        $this->parsed[$key] = $value;
    }
    
    function getItem($key){
        return $this->parsed[$key];
    }
    
    /**
Ordered by: N1 loop/segment, N101=ST, N102=Customer Name
Shipped to: N1 loop w/N101=ST; N301=Street Address, N401=City, N402=State, N403=Zip
Web order #: REF segment w/REF01=IL, REF02=Web order number
PO#: BEG segment, BEG03=PO Number
Sku #: PO1 loop(s)/segment(s), PO107=HDS SKU number
Description: PO1 loop(s), PID segment, PID05=Description
Size: PO1 loop(s)/segment(s), PO111=Size
Qty: PO1 loop(s)/segment(s), PO102=Quantity
Special order #: REF segment w/REF01=CO, REF02=Special order number
LCP contract #: PO1 loop(s), N9 loop/segment w/N901=QS, MTX segment, MTX01=ZZZ, MTX02=LCP contract number
Gift card message: N9 loop/segment w/N901=SH, MTX segment, MTX02=Gift message
The customer may enter up to 550 characters into this field. The gift message should print on the “card” section in lower right corner.
Image of the gift card included in the setup packet.

     */
    /*
    ########### HELZBERG ############## */
    function test(){
    $a =     "
        ISA*00*          *00*          *01*006965735      *01*LXGM           *170131*0751*U*00403*000194074*0*P*|~  ### DATE
        GS*PO*006965735*LXGM*20170131*0751*194148*X*004030~
        ST*850*70109~
        BEG*00*OS*1023872680**20170130~   ##PO NUM
        REF*IA*9665~
        REF*IT*09833~
        REF*CO*1053011240~
        PER*BD*Alison Elley*WP*8166271265*EM*adelley@helzberg.com~
        CSH*SC~SAC*A*A260****2*10~
        ITD*14**0**0**60*****Net 60 days~
        DTM*001*20170208~
        DTM*002*20170201~
        N9*ZZ*MTX~MTX*ZZZ*Suppliers acceptance of this Purchase Order obligates Supplier to abide by all.~
        N9*ZZ*MTX~MTX*INT*CSO#:1053011240    Store:0053 - Southlake    Cust:Sandra Lidrbauch    Need By:~
        N1*BT*HDS*92*0010~N3*1825 Swift Ave~   ### ORDER BY
        N4*N Kansas City*MO*64116~
        N1*ST*HDS*92*0053~  ### SHIP TO
        N3*1241 Southlake Mall*Georgia 54 & I-75 S.~
        N4*Morrow*GA*30260-2324~
        PO1**1*EA*12.68*WH*IN*1865256*VN*E1370101~
        CTP**RTL~
        PID*F*08***SS DVITA RD CZ MARTINI STUDS~
        CTT*1~SE*26*70109~
        GE*1*194148~IEA*1*000194074~
       

            ";
     }
    
    
       
    function parse($str){
     
        $checkStr = "*".$this::EDI_TYPE_NO."*";
        if ( strpos($str , $checkStr) !== false ){	
                        $arr_org_podate = explode("*LXGM*",$str);		
			$arr_part = explode("*", $arr_org_podate[1]);
			$org_podate = $arr_part[0];
                        
                         //Date Time Reference
			$arr_exp_date = explode("~DTM*002*",$str);
			$arr_date = explode("~",$arr_exp_date[1]);
			$req_shipdate = date("Y-m-d", strtotime($arr_date[0]));
			
                        //Date Time Reference
                        $arr_exp_date = explode("~DTM*001*",$str);
			$arr_date = explode("~",$arr_exp_date[1]);
			$req_canceldate = date("Y-m-d", strtotime($arr_date[0]));
                        
                        //
			$arr_cont = explode("*OS*",$str);
                        $arr_main = explode("**", $arr_cont[1]);
                //	$arr_info = explode("*850*",$str); 
                        $arr_cont2 = explode("*ST*", $arr_cont[1]);
////////////////////////////////////////////////////////////////////////////////////////////////////////				
                        $arr_ship = explode("*", $arr_cont2[1]); // CustName, Shiptype, ?, 
                        $arr_exp_addr1 = explode("~N3*", $arr_cont2[1]); 
                        $arr_addr1 =  explode ("~N4", $arr_exp_addr1[1] ); // Address  						
                        $arr_exp_addr2 = explode("~N4*", $arr_cont2[1]);
                        $arr_addr2 = explode("*", $arr_exp_addr2[1]);  // city , state,  zip		

                        if( preg_match('/\~REF\*IL\*([0-9]+)\~/',$str,$matches)){
                            $weborderno = $matches[1];
                        }
                        else{
                            $weborderno = "";
                        }
                       
                        //LOOP ID - PO1 :: Baseline Item Data
                        $arr_exp_product = explode("~PO1*", $arr_cont2[1]); 
                        $arr_product = explode("*", $arr_exp_product[1]);
                        // ?, unit_qty, uom, cost, ?(wh), ?(in), docno, ?(vn), itemsku, ?(iz), size(if exist), 

                        //PID :: Purchase Item Description
                        $arr_exp_product2 = explode("~PID*", $arr_cont2[1]); 
                        $arr_product2 = explode("*", $arr_exp_product2[1]);
                        //  ?(f), ?(08), ?, ?, prod desc, 
		
                        //GROUP NO
                        $arr_exp_grp = explode("|~GS*", $str); 
                        $arr_grp = explode("*", $arr_exp_grp[1]);
                        $grpno = $arr_grp[5];				

                        //PRODUCT EA
			$arr_prodinfo = explode ( "*EA*", $str );
			$arr_prodinfo2 = explode("*", $arr_prodinfo[1]);
			$itemprice = $arr_prodinfo2[0];
                        
                        
                        $packmethod = self::getShippingMethod($str);
                        
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $ship_custname    = $arr_ship[0];
                        $ship_type        = $arr_ship[1];	
                        $ship_addr1 	  = $arr_addr1[0];
                        $ship_city	  = $arr_addr2[0];
                        $ship_state	  = $arr_addr2[1];
                        $ship_zip 	  = str_replace("~PO1", "",$arr_addr2[2]);
                        
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $unit_qty = $arr_product[1];
                        $uom 	  = $arr_product[2];
                        $cost	  = $arr_product[3];  // their cost
                        $docno    = $arr_product[6]; 
                        $itemcode = $arr_product[8];  
                        $size 	  = $arr_product[10];  
                                              
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        $itemdesc = $arr_product2[4];
                        echo "<br />size : ".$size;
                        echo "<Br />description : ".$itemdesc;
                        echo "<br />";
                        $sonum = $arr_main[0];

                        //$shipcountry = $arr_info[8];				
                        //if ($shipcountry == "")
                        $shipcountry = "US";
                        $ship_addr2 = "";
                        $ship_addr3 = "";
/*					*RingSize:7.0~N9	*/

                        if(strpos($itemcode , "~CTP") !== false ){
                            $itemcode = str_replace("~CTP","",$itemcode);	
                            if(strpos($str,"RingSize:")){
                                $sarr = explode("RingSize:", $str); 
                                $size_arr = explode("~",$sarr[1]);
                                $size = $size_arr[0];
                            }else{
                                $size = "0.00";
                            }
                        }else{
                            $size = str_replace("~CTP","",$size);								
                        }
                        
                        
                        //DATE
                        $this->addItem('org_podate',$org_podate);
                        $this->addItem('req_canceldate',$req_canceldate);
                        $this->addItem('req_shipdate',$req_shipdate);
                        
                        //P.O
                        $this->addItem('grpno',$grpno);
                        $this->addItem('sonum',$sonum);
                        $this->addItem('uom',$uom);
                        $this->addItem('docno',$docno);
                       
                        //ITEM 
                        $this->addItem('unit_qty',$unit_qty);
                        $this->addItem('itemcode',$itemcode);
                        $this->addItem('size',$size); 
                        $this->addItem('itemprice',$itemprice);
                        $this->addItem('cost',$cost);//Merchant's Cost
                        $this->addItem('itemdesc',$itemdesc);
                        
                        //SHIPPING                        
                        $this->addItem('packmethod',$packmethod); 
                        $this->addItem('ship_custname',$ship_custname);
                        $this->addItem('ship_type',$ship_type);
                        $this->addItem('ship_addr1',$ship_addr1);
                        $this->addItem('ship_addr2',$ship_addr2);
                        $this->addItem('ship_addr3',$ship_addr3);
                        $this->addItem('ship_city',$ship_city);
                        $this->addItem('ship_state',$ship_state);
                        $this->addItem('ship_zip',$ship_zip);
                        $this->addItem('shipcountry',$shipcountry);
                                                
                        //CONSTANT
                        $this->addItem('num',"");
                        $this->addItem('phonenum',"");
                        $this->addItem('depid',5);
                        $this->addItem('merchant_id','helz');
                        
                        //HELZBERG
                        $this->addItem('weborderno',$weborderno);
                        print_r($this->parsed);
    
            return true;
        }else{
            return false;
        }
    }
    
    function getShippingMethod($str){
        if(strpos($str, "Overnight Delivery") !== false ){
                        $code = "UPSN";
        }else if(strpos( $str, "2-Day Delivery") !== false ){
                        $code = "UPS2";			
        }else if(strpos($str, "Ground Delivery" ) !== false ){
                        $code = "UPSG";							
        }else if(strpos( $str, "Mail") !== false ){
                        $code = "USPS";											
        }else{
                        $code = "UPSG";				
        }
        return $code;
    }
    
    

}