<?php

//error_reporting(E_ALL);
error_reporting(E_ALL);
ini_set('display_errors', 'On');

define("DROPSHIP_INBOX_URI","http://edi.elitegny.com/helz/inbox/"); //FTP Folder mapping
define("DROPSHIP_INBOX_LOG_URI","http://edi.elitegny.com/helz/inbox_log/");
define("DROPSHIP_INBOX_LOG_PATH","../../helz/inbox_log/");
define("DEBUG_MODE",false);
define("LOG_TYPE_NORMAL",0);
define("LOG_TYPE_WARN",1);
define("LOG_TYPE_INFO",2);
define("LOG_TYPE_SCRIPT",2);

//////////////////////////////////////////////////////////////
/*
"GD"  for  "Ground Delivery"
"2D"  for  "2-Day Delivery"
"1D"  for  "Overnight Delivery"
"FGD"  for  "FREE Ground Delivery"
"F1D"  for  "FREE Next Day Delivery"
2-Day Delivery
Ground Delivery
FREE Ground Delivery
FREE Next Day Delivery
FREE 2-Day Delivery
Saturday Delivery
FREE Next-Day Delivery
MAIL
*/
$is_ajax = !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';

$whitelist = array('127.0.0.1','::1','73.160.192.179','108.30.58.137'); //localhost, localhost, homdel, elite ny office
if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
    printLog("Access Denied!!",LOG_TYPE_WARN);
    die();
}

function printLog($data,$type = 0){
    global $is_ajax;
    if(!$is_ajax) {

        echo "<div style='margin:5px auto 5px auto;width:90%'>";
    }
    $can_foreach = is_array($data) || is_object($data);
    
    
    if($can_foreach) {
        echo "<ul style='font-size:11px;color:#666'>";
        foreach($data as $row){
            echo "<li>".$row."</li>";
        }
        echo "</ul>";
    }else{
        if($type == 1){
            echo "<div style='font-size:11px;color:#993300;word-break:break-all'>".$data."</div>";
        }else if($type == 2){
            echo "<div style='font-size:11px;color:#008833;word-break:break-all'>".$data."</div>";
        }else if($type == 3){
            echo "<div style='font-size:12px;color:#996600;word-break:break-all'>".$data."</div>";
        }else{
            echo "<div style='font-size:11px;color:#333;word-break:break-all'>".$data."</div>";
        }
    }
       if(!$is_ajax) {
       echo "</div>";
       
      }
}