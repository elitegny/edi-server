<?php
namespace dropship\HelzbergEDI\Invoice;
//include_once("../Nintra/Database/NintraDB.php");
//include_once("../operation/EDIFile.php");
use dropship\Nintra\Database\NintraDB as NintraDB;
use dropship\HelzbergEDI\operation\EDIFile as EDIFile;
use DateTime;
//$a = new CreateMissingEDIInvoice();
class CreateMissingEDIInvoice{
    
    public static $DB;

     static function omitted($orderno){
        self::$DB = new NintraDB() ;
        $sql =  "SELECT t1.*, t2.*,( SELECT IPDesc "
                                . "FROM TBL_ItemPrice "
                                . "WHERE IPBuyer = 'B10122' AND IPBrefersku = t2.f_itemcode ) as ipdesc "
                . "FROM TBL_SalesOrder_Ds t1, TBL_SOItemList_Ds t2 "
                . "WHERE t1.id = t2.f_orderid AND t1.departmentid = 5 AND t1.status = 4 AND t1.orderno in ($orderno) "
                . "ORDER BY t1.regdate desc;";
        //echo $sql;
        $orderno_arr = explode(',', $orderno);
        $result = self::$DB->get_query_data( $sql );
        
        $list_orderno = "";
       
        
        $round = array();
        
        foreach ( $result as $row ){
		$round[] = $row['orderno'];
        };
        
        foreach ( array_diff($orderno_arr, $round) as $row ){
            echo $row."\n";
        }
     }
     
      static function getUnregistered($orderno){
        self::$DB = new NintraDB() ;
        $sql =  "SELECT orderno "
                . "FROM TBL_SalesOrder_Ds  "
                . "WHERE orderno in ($orderno) "
                . "ORDER BY regdate desc;";
        //echo $sql;
        $orderno_arr = explode(',', $orderno);
        $result = self::$DB->get_query_data( $sql );
        $list_orderno = "";
        $round = array();
        
        foreach ( $result as $row ){
		$round[] = $row['orderno'];
        };
                $diff = array_diff($orderno_arr, $round) ;
        
        foreach ( $diff as $row ){
            echo $row.",";
        }
        
        echo "<br />"."Not in SalesOrder : ".count($diff)."<br />";
        
//        $sql =  "SELECT orderno "
//                . "FROM TBL_SalesOrder_Ds  "
//                . "WHERE orderno in ($orderno) "
//                . "ORDER BY regdate desc;";
        $sql = "SELECT COUNT(*) as total FROM tbl_missing_invoice_found WHERE inSalesOrder = 0";
        $result = self::$DB->get_query_data( $sql );
        echo "IN MISSING TABLE : ".$result[0][total];
     }
     
        
    static function generate($orderno){
        
        self::$DB = new NintraDB() ;
        
        $orderno_arr = explode(',', $orderno);
        
        $sql =  "SELECT t1.*, t2.*,( SELECT IPDesc "
                                . "FROM TBL_ItemPrice "
                                . "WHERE IPBuyer = 'B10122' AND IPBrefersku = t2.f_itemcode ) as ipdesc "
                . "FROM TBL_SalesOrder_Ds t1, TBL_SOItemList_Ds t2 "
                . "WHERE t1.id = t2.f_orderid AND t1.departmentid = 5 AND t1.status = 4 AND t1.orderno in ($orderno) "
                . "ORDER BY t1.regdate desc;";
        //]]echo $sql;
        
        $result = self::$DB->get_query_data( $sql );
        
        $list_orderno = "";
        
        $msg = "";
        
        $inv_date = date("Ymd", time());
        
        $arr_invoice = array();
        
        $round = array();
        
        self::processList($result,$round);
        
        array_diff($orderno_arr, $round);
        
        $sql = "SELECT COUNT(*) FROM tbl_missing_invoice_found WHERE inSalesOrder = 0";
        
        $result = self::$DB->get_query_data( $sql );
        
        //self::insertGeneratedEDI($arr_invoice,$orderno);

    }
    
    static function generateSingle($orderno){
        
        self::$DB = new NintraDB() ;
        
        
        
        $sql =  "SELECT t1.*, t2.*,( SELECT IPDesc "
                                . "FROM TBL_ItemPrice "
                                . "WHERE IPBuyer = 'B10122' AND IPBrefersku = t2.f_itemcode ) as ipdesc "
                . "FROM TBL_SalesOrder_Ds t1, TBL_SOItemList_Ds t2 "
                . "WHERE t1.id = t2.f_orderid AND t1.departmentid = 5 AND t1.status = 4 AND t1.orderno = $orderno "
                . "ORDER BY t1.regdate desc;";
        echo $sql;
        
        $result = self::$DB->get_query_data( $sql );
        
        $round = array();
        
        self::processList($result,$round);
        
        
        
        //self::insertGeneratedEDI($arr_invoice,$orderno);

    }
    
    static function generateToCalcMissingInvoices($orderno){
        
        self::$DB = new NintraDB() ;
        
        $orderno_arr = explode(',', $orderno);
        
        $sql =  "SELECT t1.*, t2.*,( SELECT IPDesc "
                                . "FROM TBL_ItemPrice "
                                . "WHERE IPBuyer = 'B10122' AND IPBrefersku = t2.f_itemcode ) as ipdesc "
                . "FROM TBL_SalesOrder_Ds t1, TBL_SOItemList_Ds t2 "
                . "WHERE t1.id = t2.f_orderid AND t1.departmentid = 5 AND t1.status = 4 AND t1.orderno in ($orderno) "
                . "ORDER BY t1.regdate desc;";
        //echo $sql;
        
        $result = self::$DB->get_query_data( $sql );
        
        $list_orderno = "";
        
        $msg = "";
        
        $inv_date = date("Ymd", time());
        
        $round = array();
        
        foreach ( $result as $row ){
            $round[] = $row['orderno'];
        }

        array_diff($orderno_arr, $round);

    }
    
    /*
     * 
     *   Total : 134 rows
     *    64 in SalesOrder Table
     *    70 not in salesorder
     */
    static function generateFromMissingTable(){
        
        self::$DB = new NintraDB() ;
        
        $sql =  "SELECT * FROM tbl_missing_invoice_found WHERE inSalesOrder = 0;";
                
        $result = self::$DB->get_query_data( $sql );
        
        self::processList2($result);
        
        //self::insertGeneratedEDI($arr_invoice,$orderno);

    }
    
    static function updateTrackingNo(){
        $sql = "SELECT * FROM `tbl_missing_invoice_found` as MS LEFT JOIN `tbl_salesorder_ds` as SO ON SO.orderno = MS.ponum";
        
// update SalesOrder Table from Missing Table
        $sql_update = "UPDATE tbl_missing_invoice_found AS SO
INNER JOIN tbl_salesorder_ds AS MS ON  MS.orderno = SO.ponum
SET MS.trackno = SO.tracking,
  MS.shipdate = SO.shipdate,
  MS.shippingtype = SO.shipcarrier";

// flag missing table if each order has corresponding data in sales order table
    $sql_update = "UPDATE tbl_missing_invoice_found SET inSalesOrder = 0;
            UPDATE tbl_missing_invoice_found AS MS
INNER JOIN tbl_salesorder_ds AS SO ON  SO.orderno = MS.ponum
SET MS.inSalesOrder = 1";
        
        
    $copy_ipdesc_sql = "UPDATE tbl_missing_invoice_found AS MS
INNER JOIN tbl_itemprice AS IP ON  IP.IPBuyer = 'B10122' AND IP.IPBrefersku = %MS.sellerItemNo% 
SET MS.ipdesc = IP.IPDESC;";
        
    $select_ipdesc_sql = "SELECT * FROM tbl_missing_invoice_found AS MS
INNER JOIN tbl_itemprice AS IP ON  IP.IPBuyer = 'B10122' AND IP.IPBrefersku LIKE CONCAT('%', MS.sellerItemNo, '%');";
                
                
    $select_unique_ipdesc_sql = "SELECT * FROM tbl_missing_invoice_found AS MS
INNER JOIN tbl_itemprice AS IP ON  IP.IPBuyer = 'B10122' AND IP.IPBrefersku LIKE CONCAT('%', MS.sellerItemNo, '%') 
group by ponum;";
                
    $select_update_po_date = "UPDATE tbl_missing_invoice_found AS MS
INNER JOIN tbl_helz_po_history AS HP ON  MS.ponum = HP.PO_Number 
SET MS.po_date = HP.Order_Date;";
    }
    
    static function updateShippingDate(){
        $dateList = array('7-21-2015','7-23-2015','7-24-2015','7-24-2015','8-14-2015','8-17-2015','8-17-2015','8-17-2015','8-17-2015','8-17-2015','8-17-2015','11-6-2015','11-6-2015','11-6-2015','11-6-2015','11-6-2015','11-6-2015','11-7-2015','11-7-2015','11-7-2015','11-7-2015','11-7-2015','11-7-2015','11-7-2015','11-7-2015','11-7-2015','11-9-2015','11-9-2015','11-9-2015','11-9-2015','11-10-2015','11-10-2015','11-10-2015','11-10-2015','11-10-2015','11-10-2015','11-10-2015','11-10-2015','11-10-2015','11-10-2015','11-10-2015','11-10-2015','11-11-2015','11-11-2015','11-11-2015','11-11-2015','11-11-2015','11-11-2015','11-11-2015','11-11-2015','11-11-2015','11-11-2015','11-11-2015','11-11-2015','11-11-2015','11-11-2015','11-12-2015','11-12-2015','11-16-2015','11-16-2015','11-16-2015','11-16-2015','11-16-2015','11-16-2015','11-16-2015','11-16-2015','11-16-2015','11-16-2015','11-16-2015','11-16-2015','11-16-2015','11-16-2015','11-16-2015','11-16-2015','11-16-2015','11-16-2015','11-16-2015','11-16-2015','11-16-2015','11-16-2015','12-15-2015','12-18-2015','1-29-2016','1-29-2016','2-2-2016','2-2-2016','2-2-2016','2-2-2016','3-13-2016','3-22-2016','4-25-2016','9-22-2016','9-22-2016','9-22-2016','9-22-2016','9-22-2016','10-10-2016','10-10-2016','10-10-2016','10-10-2016','10-10-2016','10-10-2016','10-10-2016','10-10-2016','10-10-2016','10-10-2016','10-10-2016','10-10-2016','10-10-2016','10-10-2016','10-14-2016','10-14-2016','10-14-2016','10-14-2016','11-16-2016','11-18-2016','11-18-2016','11-18-2016','11-18-2016','11-28-2016','11-29-2016','11-29-2016','11-30-2016','11-30-2016','12-2-2016','12-2-2016','12-7-2016','12-9-2016','12-9-2016','12-9-2016','12-9-2016','7-21-2015','7-21-2015','12-13-2015');
        
        self::$DB = new NintraDB() ;
       
//        for($i=0;$i<count($dateList);$i++){
//            $sdate =    DateTime::createFromFormat ('m-d-Y', $dateList[$i]);
//            $mysqltime = $sdate->format('Y-m-d H:i:s') ;
//            $sql = "UPDATE tbl_missing_invoice_found SET shipdate = '".$mysqltime."' WHERE id=".($i+3);
//            self::$DB->get_query_data( $sql );
//        }
        
        for($i=0;$i<count($dateList);$i++){
            $sql = "UPDATE tbl_missing_invoice_found SET invoiceno = ".(181108 + $i)." WHERE id=".($i+3);
            self::$DB->get_query_data( $sql );
        }
        
        self::$DB->close();
        
        $orderid = 181110 + $data['id'];
    }
    
    static function processList($result,&$round){
        
         foreach ( $result as $row ){
                $inv_date = date("Ymd", time());
                $round[] = $row['orderno'];
		$msg = "";	
		$orderid = $row['id'];
		$orderno = $row['orderno'];
		$org_podate = date('Ymd', strtotime($row[org_podate]));
		$short_org_podate = substr(date('Ymd', strtotime($row[org_podate])), 2, 6);	
		$shipdate = date('Ymd', strtotime($row['regdate']));
		$short_shipdate = substr(date('Ymd', strtotime($row['regdate'])), 2, 6);	
		$trackno = $row['trackno'];
		$grpno = trim($row['groupno']);
		$orderid_ninedigits = self::getStrWithDigits ( $orderid, 9 );		
		$shipcustname = $row['shipcustname'];
		$shippingtype = $row['shippingtype'];
	//	$item_desc = substr(TRIM($row['IPDesc']),0,40)."..";
		$itemcode = $row['f_itemcode'];
		
		$sz_term = "SZ";
		$sz_info = $row['size'];
		if ( $sz_info == "" ){
			$sz_term = "";
		}
		
		$arr_vendorsku = explode("_", $row[f_vendorsku]);
		$vendorsku = $arr_vendorsku[0];
	//	$item_qty = $row['IPStock'];
	//	$item_price = $row['IPPrice'];
	//	$item_size = intval($row['IPSize']);
		
		$shipaddr1 = $row['shipaddr1'];
		$shipcity = $row['shipcity'];
		$shipstate = $row['shipstate'];
		$shipzip = $row['shipzip'];

		$qty = $row['qty'];
		$price = $row['price'];
		$ipdesc = trim($row['ipdesc']);
		$invno = "HZN".$orderid;
		$trackno = $row['trackno'];
		
		
		$term_netday = 60;
		$term_netduedate = date("Ymd", time() + ( ( 60 * 60 * 60 * 24 ) * $term_netday ));
		$org_totamt = ($qty * $price * 100);
		$discount_amt = 0;
				
		if ( $discount_amt == 0 ){
			$discount_amt = "000";
			$totamt = $org_totamt - 0;			
		} else{
			$discount_amt = $discount_amt * 100;		
			$totamt = $org_totamt - $discount_amt; 			
		}
	
		$invtime = date("His", time());	
		
		if ( $grpno != "" || true){
		
                    $msg .= "ISA*00*          *00*          *01*LXGM           *01*006965735      *$short_shipdate*1202*U*00403*$orderid_ninedigits*0*T*|~";
                    $msg .= "GS*IN*LXGM*006965735*$shipdate*1202*$grpno*X*004030~";
                    $msg .= "ST*810*$orderid~";
                    //$msg .= "BIA*00*ZZ*$itemcode*$shipdate*1423~";  

                    $msg .= "BIG*$inv_date*$invno*$org_podate*$orderno~";
                    $msg .= "REF*VN*$orderno~";
                    $msg .= "PER*AR*JIN*WP*2127255049*EM*jin.m@elitegny.com~";

                    if ( $shipcustname != "HELZBERG" ){

                        $msg .= "N1*BT*HELZBERG*92*0010~";  // 
                        //$msg .= "N3*1825 SWIFT AVE.~";  // -->
                        //$msg .= "N4*NORTH KANSAS CITY*MO*64116~";
                        $msg .= "N1*ST*$shipcustname**~";  // 
                        $msg .= "N3*$shipaddr1~";  // -->
                        $msg .= "N4*$shipcity*$shipstate*$shipzip~";
                        $cnt_segments = "17";

                    }else{   // = Helzberg

                        $msg .= "N1*BT*HELZBERG*92*0010~";  // 
                        $msg .= "N1*ST***~";  // 
                        $cnt_segments = "15";
                    }

                    //$msg .= "ITD*14*3*0***$term_netduedate*$term_netday*0*0~";
                    //$msg .= "ITD*14*3****$term_netduedate*$term_netday**~";
                    $msg .= "ITD*14*3****$term_netduedate*$term_netday***~";
                    $msg .= "DTM*007*$inv_date~";

                    $cm_term = "";
                    $cm_info = "";
                    // Loop
                    //$msg .= "IT1**$qty*EA*$price*WH*IN*$vendorsku*VN*$itemcode*$cm_term*$cm_info*SZ*$sz_info~";
                    $msg .= "IT1*1*$qty*EA*$price*WH*IN*$vendorsku*VN*$itemcode*$cm_term*$cm_info*$sz_term*$sz_info~";

                    $msg .= "QTY*39*$qty~";
                    // Loop
                    //$msg .= "PID*F*$ipdesc~";
                    //	$msg .= "CAD***CN*~";

                    $msg .= "PID*F****$ipdesc~";
                   
                    if ( $shippingtype == "UPSG" ){
                            $shippingtype_str = "UPS Ground"; 
                    }
                    else if ( $shippingtype == "UPSN" ){
                            $shippingtype_str = "UPS Overnight";	
                    } 
                    else if ( $shippingtype == "UPS2" ){
                            $shippingtype_str = "UPS 2nd Days";			
                    } 

                    $msg .= "CAD****".$shippingtype."*".$shippingtype_str."**CN*$trackno~";
                    $msg .= "TDS*".$org_totamt."*".$totamt."**".$discount_amt."00~";
                    $msg .= "CTT*1~";
                    $msg .= "SE*$cnt_segments*$orderid~";					// INCLUDING ST AND SE
                    $msg .= "GE*1*$grpno~";
                    $msg .= "IEA*1*$orderid_ninedigits~";	
                    // $msg .= "IEA*1*$orderid_ninedigits~";	
                    $msg .= "\r\n";
                            //			BREAK;
		}
	
		$list_orderno .= "'$orderno',";	
                $filename = "810_".$orderno."_".$invtime.".edi";
                $arr_invoice[$orderno] = array( $msg, $filename);
                
                EDIFile::writeMissingInvoiceEDI($msg, $filename);
                //echo $filename ."<br />";
        }
    }
    
    
    static function processList2($result,&$round){
        
        foreach ( $result as $row ){
            
            $created = self::data2edi($row);

            $filename = "810_".$created['orderno']."_".date("His", time()).".edi";

            EDIFile::writeMissingInvoiceEDI($created['edi'], $filename);
        }
    }
    
    
    static function processListForCalc($result,&$round){
        
        
    }
    
    /**
     * 
     * @param type $data
     * @return type
     * It looks like we just now received these – the GS segment looks correct but there are two issues preventing the 810s from getting through our EDI translator:
 
-          The invoice date (BIG01 field) is blank, this is a required field
-          The effective date (DTM02 where DTM01=007) is blank, this is a required field if the DTM segment is present.
 
You may send corrected 810s at any time.
     */
    
     static function data2edi($data){

		$orderid = 181110 + $data['id'];
		$orderno = $data['ponum'];
                
		$org_podate = date('Ymd', strtotime($data['po_date']));
		$short_org_podate = substr(date('Ymd', strtotime($data[po_date])), 2, 6);	
		$shipdate = date('Ymd', strtotime($data['shipdate']));
		$short_shipdate = substr(date('Ymd', strtotime($data['shipdate'])), 2, 6);	
		$trackno = $data['trackno'];
	//	$grpno = trim($data['groupno']);
		$orderid_ninedigits = self::getStrWithDigits ( $orderid, 9 );		
		
                $shipcustname = $data['cusname'];
		$shippingtype = $data['shipcarrier'];
	//	$item_desc = substr(TRIM($row['IPDesc']),0,40)."..";
		$itemcode = $data['sellerItemNo'];
		
		$sz_term = "SZ";
		$sz_info = $data['size'];
		if ( $sz_info == "" ){
			$sz_term = "";
		}
		
		$arr_vendorsku = explode("_", $data['buyerSKU']);
		$vendorsku = $arr_vendorsku[0];
	//	$item_qty = $row['IPStock'];
	//	$item_price = $row['IPPrice'];
	//	$item_size = intval($row['IPSize']);
		
		$shipaddr1 = $data['add1'];
		$shipcity = $data['city'];
		$shipstate = $data['state'];
		$shipzip = $data['zip'];

		$qty = $data['qty'];
		$price = $data['price'];
		$ipdesc = trim($data['ipdesc']);
		$invno = "HZN".$orderid;
		$trackno = $data['tracking'];
		$inv_date = date("Ymd", time());
		
		$term_netday = 60;
		$term_netduedate = date("Ymd", time() + ( ( 60 * 60 * 60 * 24 ) * $term_netday ));
		$org_totamt = ($qty * $price * 100);
		$discount_amt = 0;
				
		if ( $discount_amt == 0 ){
			$discount_amt = "000";
			$totamt = $org_totamt - 0;			
		} else{
			$discount_amt = $discount_amt * 100;		
			$totamt = $org_totamt - $discount_amt; 			
		}
	
                $msg = "ISA*00*          *00*          *01*LXGM           *01*006965735      *$short_shipdate*1202*U*00403*$orderid_ninedigits*0*T*|~";
                $msg .= "GS*IN*LXGM*006965735*$shipdate*1202*$grpno*X*004030~";
                $msg .= "ST*810*$orderid~";
                //$msg .= "BIA*00*ZZ*$itemcode*$shipdate*1423~";  

                $msg .= "BIG*$inv_date*$invno*$org_podate*$orderno~";
                $msg .= "REF*VN*$orderno~";
                $msg .= "PER*AR*JIN*WP*2127255049*EM*jin.m@elitegny.com~";

                if ( $shipcustname != "HELZBERG" ){

                    $msg .= "N1*BT*HELZBERG*92*0010~";  // 
                    //$msg .= "N3*1825 SWIFT AVE.~";  // -->
                    //$msg .= "N4*NORTH KANSAS CITY*MO*64116~";
                    $msg .= "N1*ST*$shipcustname**~";  // 
                    $msg .= "N3*$shipaddr1~";  // -->
                    $msg .= "N4*$shipcity*$shipstate*$shipzip~";
                    $cnt_segments = "17";

                }else{   // = Helzberg

                    $msg .= "N1*BT*HELZBERG*92*0010~";  // 
                    $msg .= "N1*ST***~";  // 
                    $cnt_segments = "15";
                }

                //$msg .= "ITD*14*3*0***$term_netduedate*$term_netday*0*0~";
                //$msg .= "ITD*14*3****$term_netduedate*$term_netday**~";
                $msg .= "ITD*14*3****$term_netduedate*$term_netday***~";
                $msg .= "DTM*007*$inv_date~";

                $cm_term = "";
                $cm_info = "";
                // Loop
                //$msg .= "IT1**$qty*EA*$price*WH*IN*$vendorsku*VN*$itemcode*$cm_term*$cm_info*SZ*$sz_info~";
                $msg .= "IT1*1*$qty*EA*$price*WH*IN*$vendorsku*VN*$itemcode*$cm_term*$cm_info*$sz_term*$sz_info~";

                $msg .= "QTY*39*$qty~";
                // Loop
                //$msg .= "PID*F*$ipdesc~";
                //	$msg .= "CAD***CN*~";

                $msg .= "PID*F****$ipdesc~";
                
                if ( $shippingtype == "UPSG" ){
                        $shippingtype_str = "UPS Ground"; 
                }
                else if ( $shippingtype == "UPSN" ){
                        $shippingtype_str = "UPS Overnight";	
                } 
                else if ( $shippingtype == "UPS2" ){
                        $shippingtype_str = "UPS 2nd Days";			
                } 
                
                /*
                 * ##
                 * ## SUMMARY AREA
                 * ##
                 * 
                 *     CAD Carrier Detail
                 *     TDS Total Monetary Value Summary M 1
                 *     CTT Transaction Totals O 1
                 *     SE Transaction Set Trailer M 1
                 *     GE Functional Group Trailer M 1
                 *     IEA Interchange Control Trailer M 1
                 * 
                 */
                
                $msg .= "CAD****".$shippingtype."*".$shippingtype_str."**CN*$trackno~";
                $msg .= "TDS*".$org_totamt."*".$totamt."**".$discount_amt."00~";
                $msg .= "CTT*1~";
                $msg .= "SE*$cnt_segments*$orderid~";					// INCLUDING ST AND SE
                $msg .= "GE*1*$grpno~";
                $msg .= "IEA*1*$orderid_ninedigits~";	
                $msg .= "\r\n";
                            //			BREAK;
		return array('edi'=>$msg,'orderno' => $orderno);
                
    }
    
    static function insertGeneratedEDI($inv,$orderno){

        $sql_add = "";
        foreach ( $inv as $orderno => $arr ){
                $msg = $arr[0];
                $fname = $arr[1];
                $sql_add .= "('$fname', '$msg', now(), 5, 'helz', '$orderno', 0, '810', 'The Elitegroup sent 810 EDI invoices to k2b.'),";
        }
        $sql_add =substr( $sql_add, 0, -1);
       
        $sql = "INSERT INTO TBL_EDI_List "
                . "(  fname, fcontent, regdate, cust_no, cust_name, orderno, status, fcate, memo ) "
                . "VALUES ". $sql_add;

        self::$DB->update($sql);
    }


    static function getStrWithDigits ( $num, $totalcnt ){

            $len = strlen($num);
            $first_zero_strcnt = $totalcnt - $len;
            $i = 1;
            $first_zero = "";
            for ( $i = 1;$i<=$first_zero_strcnt; $i++ ){

                    $first_zero .= "0";
            }

            return $first_zero.$num;
    }
}

?>