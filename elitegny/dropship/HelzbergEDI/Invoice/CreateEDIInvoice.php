<?php
namespace dropship\HelzbergEDI\Invoice;
include_once("../Nintra/Database/NintraDB.php");
include_once("../operation/EDIFile.php");
use dropship\Nintra\Database\NintraDB;

class CreateEDIInvoice{
    
    public static $DB;

    static function generate($orderno){
        self::$DB = new NintraDB() ;
        $sql =  "SELECT t1.*, t2.*,( SELECT IPDesc "
                                . "FROM TBL_ItemPrice "
                                . "WHERE IPBuyer = 'B10122' AND IPBrefersku = t2.f_itemcode ) as ipdesc "
                . "FROM TBL_SalesOrder_Ds t1, TBL_SOItemList_Ds t2 "
                . "WHERE t1.id = t2.f_orderid AND t1.departmentid = 5 AND t1.status = 4 AND t1.orderno in ($orderno) "
                . "ORDER BY t1.regdate desc;";
        
        $result = self::$DB->get_query_data( $sql );
        
        $list_orderno = "";
        $msg = "";
        $inv_date = date("Ymd", time());
        $arr_invoice = array();
        
        foreach ( $result as $row ){
		$msg = "";	
		$orderid = $row['id'];
		$orderno = $row['orderno'];
		$org_podate = date('Ymd', strtotime($row[org_podate]));
		$short_org_podate = substr(date('Ymd', strtotime($row[org_podate])), 2, 6);	
		$shipdate = date('Ymd', strtotime($row['regdate']));
		$short_shipdate = substr(date('Ymd', strtotime($row['regdate'])), 2, 6);	
		$trackno = $row['trackno'];
		$grpno = trim($row['groupno']);
		$orderid_ninedigits = self::getStrWithDigits ( $orderid, 9 );		
		$shipcustname = $row['shipcustname'];
		$shippingtype = $row['shippingtype'];
	//	$item_desc = substr(TRIM($row['IPDesc']),0,40)."..";
		$itemcode = $row['f_itemcode'];
		
		$sz_term = "SZ";
		$sz_info = $row['size'];
		if ( $sz_info == "" ){
			$sz_term = "";
		}
		
		$arr_vendorsku = explode("_", $row[f_vendorsku]);
		$vendorsku = $arr_vendorsku[0];
	//	$item_qty = $row['IPStock'];
	//	$item_price = $row['IPPrice'];
	//	$item_size = intval($row['IPSize']);
		
		$shipaddr1 = $row['shipaddr1'];
		$shipcity = $row['shipcity'];
		$shipstate = $row['shipstate'];
		$shipzip = $row['shipzip'];

		$qty = $row['qty'];
		$price = $row['price'];
		$ipdesc = trim($row['ipdesc']);
		$invno = "HZN".$orderid;
		$trackno = $row['trackno'];
		
		
		$term_netday = 60;
		$term_netduedate = date("Ymd", time() + ( ( 60 * 60 * 60 * 24 ) * $term_netday ));
		$org_totamt = ($qty * $price * 100);
		$discount_amt = 0;
				
		if ( $discount_amt == 0 ){
			$discount_amt = "000";
			$totamt = $org_totamt - 0;			
		} else{
			$discount_amt = $discount_amt * 100;		
			$totamt = $org_totamt - $discount_amt; 			
		}
	
		$invtime = date("His", time());	
		
		if ( $grpno != "" ){
                    //shipdate 
                    $msg .= "ISA*00*          *00*          *01*LXGM           *01*006965735      *$short_shipdate*1202*U*00403*$orderid_ninedigits*0*T*|~";
                    $msg .= "GS*IN*LXGM*006965735*$shipdate*1202*$grpno*X*004030~";
                    $msg .= "ST*810*$orderid~";
                    //$msg .= "BIA*00*ZZ*$itemcode*$shipdate*1423~";  

                    $msg .= "BIG*$inv_date*$invno*$org_podate*$orderno~";
                    $msg .= "REF*VN*$orderno~";
                    $msg .= "PER*AR*JIN*WP*2127255049*EM*jin.m@elitegny.com~";

                    if ( $shipcustname != "HELZBERG" ){

                        $msg .= "N1*BT*HELZBERG*92*0010~";  // 
                        //$msg .= "N3*1825 SWIFT AVE.~";  // -->
                        //$msg .= "N4*NORTH KANSAS CITY*MO*64116~";
                        $msg .= "N1*ST*$shipcustname**~";  // 
                        $msg .= "N3*$shipaddr1~";  // -->
                        $msg .= "N4*$shipcity*$shipstate*$shipzip~";
                        $cnt_segments = "17";

                    }else{   // = Helzberg

                        $msg .= "N1*BT*HELZBERG*92*0010~";  // 
                        $msg .= "N1*ST***~";  // 
                        $cnt_segments = "15";
                    }

                    //$msg .= "ITD*14*3*0***$term_netduedate*$term_netday*0*0~";
                    //$msg .= "ITD*14*3****$term_netduedate*$term_netday**~";
                    $msg .= "ITD*14*3****$term_netduedate*$term_netday***~";
                    $msg .= "DTM*007*$inv_date~";

                    $cm_term = "";
                    $cm_info = "";
                    // Loop
                    //$msg .= "IT1**$qty*EA*$price*WH*IN*$vendorsku*VN*$itemcode*$cm_term*$cm_info*SZ*$sz_info~";
                    $msg .= "IT1*1*$qty*EA*$price*WH*IN*$vendorsku*VN*$itemcode*$cm_term*$cm_info*$sz_term*$sz_info~";

                    $msg .= "QTY*39*$qty~";
                    // Loop
                    //$msg .= "PID*F*$ipdesc~";
                    //	$msg .= "CAD***CN*~";

                    $msg .= "PID*F****$ipdesc~";
                    //	$msg .= "CAD****UPGF*UPS FREIGHT**CN*$trackno~";
                    if ( $shippingtype == "UPSG" ){
                            $shippingtype_str = "UPS Ground"; 
                    }
                    else if ( $shippingtype == "UPSN" ){
                            $shippingtype_str = "UPS Overnight";	
                    } 
                    else if ( $shippingtype == "UPS2" ){
                            $shippingtype_str = "UPS 2nd Days";			
                    } 

                    $msg .= "CAD****".$shippingtype."*".$shippingtype_str."**CN*$trackno~";
                    $msg .= "TDS*".$org_totamt."*".$totamt."**".$discount_amt."00~";
                    $msg .= "CTT*1~";
                    $msg .= "SE*$cnt_segments*$orderid~";					// INCLUDING ST AND SE
                    $msg .= "GE*1*$grpno~";
                    $msg .= "IEA*1*$orderid_ninedigits~";	
                    // $msg .= "IEA*1*$orderid_ninedigits~";	
                    $msg .= "\r\n";
                            //			BREAK;
		}
	
		$list_orderno .= "'$orderno',";	
                $filename = "810_".$orderno."_".$invtime.".edi";
                $arr_invoice[$orderno] = array( $msg, $filename);
            
                EDIFile::writeInvoiceEDI($msg, $filename);
        }
        
        self::insertGeneratedEDI($arr_invoice,$orderno);

    }
    
    static function insertGeneratedEDI($inv,$orderno){

        $sql_add = "";
        foreach ( $inv as $orderno => $arr ){
                $msg = $arr[0];
                $fname = $arr[1];
                $sql_add .= "('$fname', '$msg', now(), 5, 'helz', '$orderno', 0, '810', 'The Elitegroup sent 810 EDI invoices to k2b.'),";
        }
        $sql_add =substr( $sql_add, 0, -1);
       
        $sql = "INSERT INTO TBL_EDI_List "
                . "(  fname, fcontent, regdate, cust_no, cust_name, orderno, status, fcate, memo ) "
                . "VALUES ". $sql_add;

        self::$DB->update($sql);
    }


    static function getStrWithDigits ( $num, $totalcnt ){

            $len = strlen($num);
            $first_zero_strcnt = $totalcnt - $len;
            $i = 1;
            $first_zero = "";
            for ( $i = 1;$i<=$first_zero_strcnt; $i++ ){

                    $first_zero .= "0";
            }

            return $first_zero.$num;
    }
}

?>