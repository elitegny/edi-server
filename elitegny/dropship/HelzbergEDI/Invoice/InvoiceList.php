<?php

namespace dropship\HelzbergEDI\Invoice;
include_once("../../Nintra/Database/NintraDB.php");
use dropship\Nintra\Database\NintraDB;
use mysqli;

class InvoiceList{
    
    static $mysqli;
    
    public static function generate(){
        
        self::$mysqli = new mysqli("nintra.elitegny.com", "elitegny", "elitegny", "egnymasterdb");
     
        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            return false;
        }
        if(DEBUG_MODE){
             $ids = self::test_makeQuery();
            return self::test_getInvoiceList($ids);
        }else{
            $ids = self::makeQuery();
            return self::getInvoiceList($ids);
        }
    }
    
    static function updateSalesOrder2Invoice($sql){
      if (self::$mysqli->multi_query($sql)) {
            do {
                if ($result = self::$mysqli->store_result()) {
                    while ($row = $result->fetch_row()) {
                        printf("%s\n", $row[0]);
                    }
                    $result->free();
                }
          
                if (self::$mysqli->more_results()) {
                    printf("-----------------\n");
                }
            } while (self::$mysqli->next_result());
        }  
    }
    
    static function test_makeQuery(){

        $inv_from = date("Y-m-d", time() - ( 86400 * 7 * 2 ));
        $inv_to = date("Y-m-d", time() - ( 86400 * 7 ) );

        $sql = "SELECT distinct t2.id, t2.f_orderid, t2.f_sonum "
                . "FROM TBL_SalesOrder_Ds t1, TBL_SOItemList_Ds t2 "
                . "WHERE t1.id = t2.f_orderid "
                . "AND t1.departmentid = 5 "
                . "AND t1.status = 4 "
//                . "AND ( t1.inv_requested='N' OR t1.inv_confirmed <> 'Y' ) "
                . "AND ( t1.org_podate > '$inv_from' "
                . "AND t1.org_podate < '$inv_to' );";
        
        if(!$result = self::$mysqli->query($sql)){
            echo $sql."<br />";
            die('There was an error running the query [' . self::$mysqli->error . ']');
        }

        $list_id = "";
        
        while($row = $result->fetch_assoc()){
            $list_id = $list_id. $row['id']. ",";
            $multi_sql .= "call updateHelzSalesOrder2Invoice(".$row['id'].");";
        }
        
        $list_id = substr($list_id, 0, -1);
        
        //self::updateSalesOrder2Invoice($multi_sql);
        
        return $list_id;
    }
    
    public static function test_getInvoiceList($ids){
        $sql =  "SELECT t1.orderno "
            . "FROM TBL_SalesOrder_Ds t1, TBL_SOItemList_Ds t2 "
            . "WHERE t1.id = t2.f_orderid "
                . "AND t1.departmentid = 5 "
//                . "AND t1.status = 4 "
//                . "AND (t1.inv_requested='Y' OR t1.inv_confirmed <> 'Y') "
                . "AND  t2.id in ( $ids );";
 
  
        if(!$result = self::$mysqli->query($sql)){
            die('There was an error running the query [' . self::$mysqli->error . ']');
        }
        
        if ( $result->num_rows == 0 ){
                echo "Not invoiced in our system";
                return false;
        }
        $list_orderno = "";
        while($row = $result->fetch_assoc()){
            $list_orderno .= $row[orderno]."\r\n";
        }
        self::$mysqli->close();
        return $list_orderno;
    }
    
    static function makeQuery(){

        $inv_todaydate = date("Y-m-d", time());
        $inv_tommdate = date("Y-m-d", time() + 84650);
        $inv_twoweeks = date("Y-m-d", time() - ( 84650 * 7 ) );

        $sql = "SELECT distinct t2.id, t2.f_orderid, t2.f_sonum "
                . "FROM TBL_SalesOrder_Ds t1, TBL_SOItemList_Ds t2 "
                . "WHERE t1.id = t2.f_orderid "
                . "AND t1.departmentid = 5 "
                . "AND t1.status = 4 "
                . "AND ( t1.inv_requested='N' OR t1.inv_confirmed <> 'Y' ) "
                . "AND ( t1.org_podate > '2016-06-01' "
                . "AND t1.org_podate < '$inv_twoweeks' );";

        if(!$result = self::$mysqli->query($sql)){
            die('There was an error running the query [' . self::$mysqli->error . ']');
        }

        $list_id = "";
        
        while($row = $result->fetch_assoc()){
            $list_id = $list_id. $row['id']. ",";
            $multi_sql .= "call updateHelzSalesOrder2Invoice(".$row['id'].");";
        }
        
        $list_id = substr($list_id, 0, -1);
        
        self::updateSalesOrder2Invoice($multi_sql);
        
        return $list_id;
    }
       
    public static function getInvoiceList($ids){
        $sql =  "SELECT t1.orderno "
            . "FROM TBL_SalesOrder_Ds t1, TBL_SOItemList_Ds t2 "
            . "WHERE t1.id = t2.f_orderid "
                . "AND t1.departmentid = 5 "
                . "AND t1.status = 4 "
                . "AND (t1.inv_requested='Y' OR t1.inv_confirmed <> 'Y') "
                . "AND  t2.id in ( $ids );";
 
  
        if(!$result = self::$mysqli->query($sql)){
            die('There was an error running the query [' . self::$mysqli->error . ']');
        }
        
        if ( $result->num_rows == 0 ){
                echo "Not invoiced in our system";
                exit;
        }

        $list_orderno = "";
        while($row = $result->fetch_assoc()){
            $list_orderno .= $row[orderno]."\r\n";
        }

        self::$mysqli->close();

        return $list_orderno;
    }

}
?>