<?php

namespace dropship\HelzbergEDI\operation;

class EDIFile
{
    
    const BATCH_EDI_INBOX_2_WEB_INBOX = '../script/web/edi_move_to_log.bat';
    const BATCH_EDI_INBOX_DIR = "../script/web/dir_helz_inbox.bat";
    const BATCH_EDI_LOG_DIR = "../script/web/dir_helz_logfolder.bat";
    const BATCH_MISSING_INVOICE_2_OUTBOX = "../script/onetime_810_missing_invoice_export.bat";
    
    const WEB_INBOX_URI = "http://edi.elitegny.com/helz/inbox/";
            
    public static function getInboxDirectoryList(){
       
        return self::executeScript(realpath(self::BATCH_EDI_INBOX_DIR), true);
        //return $output;
    }
    
    public static function getLogFolderList(){
        return self::executeScript(realpath(self::BATCH_EDI_LOG_DIR), true);
       // return $output;
    }
    
    public static function move_missing_invoice_to_outbox(){
        echo realpath(self::BATCH_MISSING_INVOICE_2_OUTBOX);
        return self::executeScript(realpath(self::BATCH_MISSING_INVOICE_2_OUTBOX));
    }
    
    public static function move_inbox_file_to_log($filename, $edi_docno, $orderno){
       return self::executeScript(realpath(self::BATCH_EDI_INBOX_2_WEB_INBOX)." $filename $edi_docno"."_".$orderno."_"."$filename", false);
    }
    
    public static function import_860($filename, $edi_docno, $orderno){
	return self::executeScript(realpath(self::BATCH_EDI_INBOX_2_WEB_INBOX)." $filename $edi_docno"."_".$orderno."_"."$filename", false);
    }

    public static function import($filename, $edi_docno){
        return self::executeScript(realpath(self::BATCH_EDI_INBOX_2_WEB_INBOX)." $filename $edi_docno"."_"."$filename", false);
    }
    
    public static function import_997($filename, $edi_docno, $orderno){
        return self::executeScript(realpath(self::BATCH_EDI_INBOX_2_WEB_INBOX)." $filename $edi_docno"."_".$orderno."_"."$filename", false);
    }
    
    public static function writeInvoiceEDI($edi,$filename){
        self::write ( "../outbox_log/".$filename, $edi );
    }
    public static function writeMissingInvoiceEDI($edi,$filename){
        self::write ( "../outbox_missing_log/".$filename, $edi );
    }
    public static function writeShipNotice($edi,$filename){
        self::write( "../outbox_log_856/".$filename, $edi );
    }
    
    public static function writeInvoiceList($content){
  //////////// Generate "list_of_ordernos_invoicing.txt" /////////////////////
        self::write ( "../helz/list_of_ordernos_invoicing.txt", $content );
    }
    
    public static function writeInventory($content){
        $sdate = date('Y-m-d', time()); // since date

        self::write("../helz/outbox_log/846_inventory_$sdate.edi", $content );
    }
            
    public static function write($file, $data){
        if(DEBUG_MODE){
            printLog($file);
            printLog($data);
        }
        else{
            $Handle = fopen($file, 'w');
            $result = fwrite($Handle, $data); 
             if ($result === false) {
                 print "<span style='color:red'>$file Data failed to write</span>";
             }else{
                 print "$file Data Written <hr />"; 
             }
            fclose($Handle);
        }
    }
    
    public static function executeScript($script,$hasOutput){
        if(DEBUG_MODE){
            printLog($script,LOG_TYPE_SCRIPT);
        }else{
              echo file_exists($script);
            if($hasOutput){
              
                exec($script,$output);
             
                return $output;
            }else{
                echo "-----".$script;
                exec($script);
                 echo $output;
            }
           
        }
    }
    /* 
     * Public folder is not accessable 
     */
    public static function getFileList($dir){
        $files = array();
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    $files[]= $file;
                }
                closedir($dh);
            }
        }
        return $files;
    }
    
}