<?php


namespace dropship\HelzbergEDI;

//include_once("../Nintra/Database/NintraDB.php");
//include_once("./EDIDatabase.php");
//include_once("./EDIFile.php");

use dropship\HelzbergEDI\PurchaseOrder\PoEdiParser as PoEdiParser;
use dropship\Nintra\Database\NintraDB AS NintraDB;

class EDIHandler
{
    
    public $DB;
    
    
    function EDIHandler(){
        $this->DB = new NintraDB();
    }
    
    function importEdi997ToDB($path, $orderno, $memo) {

       
        /* 	
          $arr_path = explode( "/",  $path);
          $filename = $arr_path [ count( $arr_path ) -1  ];

          $receipt_edi_no = substr( $memo, 0, 3);
          $sql = "update TBL_EDI_List
          set
          fname = '997_".$receipt_edi_no."_".$orderno."_".$filename."',
          orderno = '$orderno',
          fcate = '997_".$receipt_edi_no."',
          memo = '$memo'
          where org_fname = '$filename'";

          echo $sql;
          //function importEdi997InboxToDB($path, "997", $orderno, "PO: $orderno Ship-Notice has been received by K2B."){
          //			move_inbox_file_to_log($filename, "997");
          $db->update($sql);
         */
    }

    function importEdi860ToDB($path, $orderno, $memo) {

        $db = new NintraDB();
        /* 	
          $arr_path = explode( "/",  $path);
          $filename = $arr_path [ count( $arr_path ) -1  ];
          $receipt_edi_no = substr( $memo, 0, 3);

          $sql = "update TBL_EDI_List
          set
          fname = '860_".$orderno."_".$filename."',         orderno = '$orderno',         fcate = '860',         memo = '$memo'
          where org_fname = '$filename'";

          //	echo $sql;
          //function importEdi997InboxToDB($path, "997", $orderno, "PO: $orderno Ship-Notice has been received by K2B."){
          //			move_inbox_file_to_log($filename, "997");
          $db->update($sql);
         */


        $sql = "update TBL_SalsOrder_Ds t1, TBL_SOItemList_Ds t2 set 
			t1.status = -1, t2.status = -1 where t1.id =t2.f_orderid and t1.orderno = '$ordno';";
        
        $db->update($sql);
        $db->close();
        
    }
    
    function importPurchaseOrder($filename ){

                $edi_content = file_get_contents( DROPSHIP_INBOX_URI.$filename );
			
                $parser = new PoEdiParser();
                $parsed = $parser->parse($edi_content);
                
                if($parsed){
                    $this->insertSalesOrder($parser,$affected_soid, $isNewOrder);

                    if ( $isNewOrder == 1 ){
                        $this->insertSalesOrderItem_helz($parser, $affected_soid);
                        echo "<br />New Order & Item inserted -> OK";
                    }else{
                        echo "<br /><span style='color:red'>".$parser->getItem('sonum')." Order already existed.</span>";  
                    }
                    
                   // EDIFile::move_inbox_file_to_log($filename, "850");
		}
    }
    
    function insertSalesOrder($parser, &$affected_soid, &$isNewOrder){
                 
        $g = $parser->getCollection();
	
        $sl_ship_addr1 = addslashes($g['ship_addr1']);
        $sl_ship_city = addslashes($g['ship_city']);
        $sl_ship_custname = str_replace("'", "\'", $g['ship_custname']);
          
        
        $sql = "SELECT id FROM TBL_SalesOrder_Ds WHERE orderno = '$g[sonum]' LIMIT 1";
        $rows = $this->DB->get_query_data( $sql );
	$isNewOrder =  count($rows) == 0;
        
        if ( $isNewOrder ){ 
            $sql = "INSERT INTO TBL_SalesOrder_Ds "
                    . "(orderno,  regdate,  shiptoid,  type,  status,  salestax,  statuschanged,  departmentid,  total,  lastchanged,  shippingtype,  docno,qbid, term, shipdate, aftercanceldate, requestshipdate, closed, groupno, shipcharge, shipcustname, shipaddr1, shipaddr2, shipaddr3, shipcity, shipstate, shipzip,shipcountry,phonenum,req_shipdate,req_canceldate,org_podate) "
                    . "VALUES "
                    . "('$g[sonum]',   now(),   1,   1,   0,   0,   null,   $g[depid],   0,   now(),   '$g[packmethod]', '$g[docno]', 'qbid', 'term', null,null, null, 0,'$g[grpno]', null,'$sl_ship_custname','$sl_ship_addr1','$g[ship_addr2]','$g[ship_addr3]','$sl_ship_city','$g[ship_state]','$g[ship_zip]','$g[shipcountry]','$g[phonenum]','$g[req_shipdate]','$g[req_canceldate]','$g[org_podate]');";

            $this->DB->update($sql);
            $affected_soid = mysql_insert_id();
        }else{
            $affected_soid = $rows[0]['id'];
        }
    }

    function insertSalesOrderItem($parser,$affected_id){
        
        $g  = $parser->getCollection();
        $item = $this->getItemPrice($g);
        $price = $item[0][IPPrice];
        $f_itemid = $item[0][IPid];
        $f_mercode = $item[0][IPBuyer];
        $f_size = $item[0][IPSize];
        $f_vendorsku = $item[0][IPBsku];
        $f_itemcode = $arr_iteminfo[0][IPBrefersku];
        $cust_id = $this->getIPBuyerCode($g[merchant_id]);

        $sql = "INSERT INTO TBL_SOItemList_Ds (f_orderid, price, qty, f_itemid, status, shipeddate,trackno, seqno, size, f_sonum, f_vendorsku,f_itemcode, so_regdate, f_depnum,cust_str,ext1,cust_id, active) VALUES ( $affected_id, $g[price],  $g[unit_qty], $g[f_itemid], 1, now(),'', null, '$g[f_size]', '$g[sonum]', '$f_vendorsku','$f_itemcode',now(),'$f_mercode','$g[merchant_id]','$g[num]','$cust_id', 1 );";
        $this->DB->update($sql);

        $sql = "UPDATE TBL_SalesOrder_Ds SET total = total + $g[unit_qty] WHERE id = $soid;";
        $this->DB->update($sql);

    }


    function insertSalesOrderItem_helz($parser, $affected_id){
        
        $g = $parser->getCollection();
        $item = $this->getItemPrice($g);
        $f_itemid = $item[0][IPid];
        $f_mercode = $item[0][IPBuyer];
        $f_size = $item[0][IPSize];
        $f_vendorsku = $item[0][IPBsku];
        $f_itemcode = $item[0][IPBrefersku];
        $cust_id = $this->getIPBuyerCode($g[merchant_id]);     
        
        $sql = "INSERT INTO TBL_SOItemList_Ds (f_orderid, price, qty, f_itemid, status, shipeddate,trackno, seqno, size, f_sonum, f_vendorsku,f_itemcode, so_regdate, f_depnum,cust_str,ext1,cust_id, active ) VALUES ($affected_id, $g[itemprice],  $g[unit_qty], $f_itemid, 1, now(),'', null, '$f_size', '$g[sonum]', '$f_vendorsku','$f_itemcode',now(),'$f_mercode','$g[merchant_id]','$g[num]','$cust_id', 1 );";
        $this->DB->update($sql);
        
        $sql = "UPDATE TBL_SalesOrder_Ds SET total = total + $g[unit_qty] WHERE id = $affected_id;";
        $this->DB->update($sql);
    }

    function getIPBuyerCode($itemcode, $f_merid){
            if ( $f_merid == "sears")
            return 'B10133';
            if ( $f_merid == "iqvc")
            return 'B20009';
            if ( $f_merid == "jcpenney")
            return 'B10007';
            if ( $f_merid == "helz" )
            return 'B10122';
    }

    function getItemPrice($collection){
        
        $merchant_id = $collection[merchant_id];
        $doc_no = $collection[docno];
        $size = $collection[size];
        $itemcode = $collection[itemcode];
        $ipbuyercode = $this->getIPBuyerCode($merchant_id);
        
        $sql = "SELECT IPTSku,IPBsku, IPSize, IPBuyer, IPBrefersku,IPid,IPPrice,IPCost "
                . "FROM TBL_ItemPrice "
                . "WHERE IPBuyerType='B' "
                . "AND IPBuyer = '$ipbuyercode'";

        if ($merchant_id == "iqvc" || $merchant_id == "sears") {
            $sql = $sql. " AND IPTSku = '$itemcode'";
        }else if ($merchant_id == "jcpenney" ){
            $sql = $sql. " AND IPBsku = '$docno'";
        }else if ($merchant_id == "helz" ){
            if( substr($itemcode, 0, 1) == "R"){
                $size = intval($size);
                $sql = $sql. " AND IPBsku LIKE '$doc_no"."_".$size."%' LIMIT 1;";
            }else{
                $sql = $sql. " AND IPBsku LIKE '$doc_no%' LIMIT 1;";	
            }
        }

        return $this->DB->get_query_data( $sql );
	
    }
}