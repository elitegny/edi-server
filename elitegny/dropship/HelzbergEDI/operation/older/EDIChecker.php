<?php

namespace dropship\HelzbergEDI;

include_once( "./EDIHandler.php");
include_once("./EDIFile.php");

use dropship\HelzbergEDI\EDIHandler;
use dropship\HelzbergEDI\EDIFile;

class EDIChecker{
    
    protected static $filename ;

    public static function check($filename){
                
                self::$filename = $filename;
		
                $content = file_get_contents( DROPSHIP_INBOX_URI.$filename );
		
                // functional receipt after sending 810 or 856 or 846
                if ( strpos($content , "*997*") !== false ){	
                    //echo "PROCESS 997";
                    self::process997();
                    
		//EDI 824 Application Advice:		
		}else if( strpos($content , "*824*") !== false ){
                    //echo "PROCESS 824";
                    self::process824();
			
		}else if ( strpos($content , "*860*") !== false ){
                    //echo "PROCESS 860";
                    self::process860();
	
                /* PURCHASE ORDER */
		}else if ( strpos($content , "*850*") !== false ){ 
                    //echo "PROCESS PO 850";
                    EDIHandler::importPurchaseOrder($filename );
		
		}
    }
    
    //EDI 997 Functional Acknowledgment: 
    // => serves as a receipt, to acknowledge that an EDI transaction
    
    protected static function Process997($edi_str){
                        
                if ( strpos($edi_str , "*856*") !== false ){
                    self::process997_856($edi_str);
                }

                if ( strpos($edi_str , "*810*") !== false ){
                    self::process997_810($edi_str);

                }		
    }
    
    //EDI 856 Ship Notice/Manifest:
    
    protected static function process997_856($edi_str){
        
        $arr_rows = explode( "*856*", $edi_str );
        $arr_orderinfo = explode ( "~", $arr_rows[1]);
        $orderno = $arr_orderinfo[0];
        EDIFile::import_997(self::$filename, "997_856", $orderno);
    }
    
    
    //EDI 997 -  FA Invoice RECEIPT:
    protected static function process997_810($edi_str){
        
        $arr_rows = explode( "*810*", $edi_str );
        $arr_orderinfo = explode ( "~", $arr_rows[1]);
        $orderno = $arr_orderinfo[0];
        EDIFile::import_997(self::$filename, "997_810", $orderno);
    }
    
    //EDI 824 Application Advice:
    //The X12 824 transaction set is the electronic version of an Application Advice document, 
    //used to notify the sender of a previous transaction that the document has been accepted, or to report on error
    
    protected static function Process824($edi_str){
        
        EDIFile::import(self::$filename, "824");				
    }
			
    
    //EDI 860 Purchase Order Change Request
    
    protected static function Process860($edi_str){
        
        EDIFile::import(self::$filename, "860");
        $arr_rows = explode( "BCH*", $edi_str );
        $arr_orderinfo = explode ( "*", $arr_rows[1]);
        $orderno = $arr_orderinfo[2];
        EDIFile::import_860(self::$filename, "860", $orderno);
	
    }	

    
}