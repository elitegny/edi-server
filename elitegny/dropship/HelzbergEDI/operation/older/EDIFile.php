<?php

namespace dropship\HelzbergEDI;
echo "EDIFILE LOADED";
class EDIFile
{
    
    const BATCH_EDI_INBOX_2_WEB_INBOX = '../script/web/edi_move_to_log.bat';
    const BATCH_EDI_INBOX_DIR = "../script/web/dir_helz_inbox.bat";
    const WEB_INBOX_URI = "http://edi.elitegny.com/helz/inbox/";
            
    public static function save(){
        
        
    }
    
    public static function update(){
        
    }
    
    public static function getInboxDirectoryList(){
        exec(realpath(self::BATCH_EDI_INBOX_DIR), $output);
        return $output;
    }
    public static function import_860($filename, $edi_docno, $orderno){
	exec(realpath(self::BATCH_EDI_INBOX_2_WEB_INBOX)." $filename $edi_docno"."_".$orderno."_"."$filename", $output);
    }

    function import($filename, $edi_docno, $sonum){
        exec(realpath(self::BATCH_EDI_INBOX_2_WEB_INBOX)." $filename $edi_docno"."_".$sonum."_"."$filename", $output);
    }
    
    function import_997($filename, $edi_docno, $orderno){
        exec(realpath(self::BATCH_EDI_INBOX_2_WEB_INBOX)." $filename $edi_docno"."_".$orderno."_"."$filename", $output);
    }
    
    public static function writeInvoiceEDI($edi,$filename){
    
        self::write ( "outbox_log/".$filename, $edi );
    }
     public static function writeMissingInvoiceEDI($edi,$filename){
        self::write ( "../helz/outbox_missing_log/".$filename, $edi );
    }
    
     public static function writeShipNotice($edi,$filename){
            
        self::write( "outbox_log/".$filename, $edi );
    }
    
    public static function writeInvoiceList($content){
  //////////// Generate "list_of_ordernos_invoicing.txt" /////////////////////
        self::write ( "../helz/list_of_ordernos_invoicing.txt", $content );
    }
    
    public static function writeInventory($content){
        $sdate = date('Y-m-d', time()); // since date

        self::write("../helz/outbox_log/846_inventory_$sdate.edi", $content );
    }
    
   
            
    public static function write($file, $data){
        
        $Handle = fopen($file, 'w');
	$result = fwrite($Handle, $data); 
         if ($result === false) {
             print "<span style='color:red'>$file Data failed to write</span>";
         }else{
             print "$file Data Written <hr />"; 
         }
	fclose($Handle); 
    }
    
    /* 
     * Public folder is not accessable 
     */
    public static function getFileList($dir){
        $files = array();
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    $files[]= $file;
                }
                closedir($dh);
            }
        }
        return $files;
    }
    
}