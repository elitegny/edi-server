<?php


namespace dropship\HelzbergEDI\operation;

//include_once("../Nintra/Database/NintraDB.php");
//include_once("./EDIDatabase.php");
//include_once("./EDIFile.php");

use dropship\HelzbergEDI\PurchaseOrder\PoEdiParser as PoEdiParser;
use dropship\Nintra\Database\NintraDB AS NintraDB;

class EDIHandler
{
    
    public $DB;
    
    function __construct() {
             $this->DB = new NintraDB();
    }
    
    function addToEDIList($ofilename,$nfilename,$content,$cusNo,$cusName,$status,$memo,$categoryCode){
         $sql = "insert  ignore into TBL_EDI_List ( org_fname, fname, fcontent, regdate, cust_no, cust_name, status , memo,fcate) values (".
		$filename .", '', ".addslashes($content).", now(),5, 'helz', 1);";
         $this->DB->update($sql);
    }
      
     
    function notifiedShipNoticeReception($orderno){
        $sql = "UPDATE  TBL_SalesOrder_Ds SET ship_confirmed = 'Y' 
		WHERE departmentid = 5 AND STATUS = 4 AND 
		orderno IN ( $orderno )";
         $this->DB->update($sql);
        
    }
    
    function notifiedInvoiceAccepted($orderno){
        $sql = "UPDATE  TBL_SalesOrder_Ds SET inv_finished = 'A' 
			WHERE departmentid = 5 AND
			STATUS = 4 AND  ( inv_finished <> 'A'  ) AND 
			orderno IN ( $orderno )";
        $this->DB->update($sql);
    }
    
    function notifiedInvoiceRejected($orderno){
        $sql = "UPDATE  TBL_SalesOrder_Ds SET inv_finished = 'R' 
			WHERE departmentid = 5 AND
			STATUS = 4 AND  ( inv_finished IS NULL OR  inv_finished = '' OR inv_finished = 'N' ) AND 
			orderno IN ( $orderno )";
        $this->DB->update($sql);
    }
    
    function notifiedInvoiceReception($orderno){
        $sql = "UPDATE  TBL_SalesOrder_Ds SET inv_confirmed = 'Y' 
		WHERE departmentid = 5 AND
		STATUS = 4 AND ( inv_confirmed IS NULL OR  inv_confirmed = '' OR inv_confirmed = 'N' )  AND 
		orderno IN ( $orderno );";
        $this->DB->update($sql);
    }
    
    //860
    function orderChangeRequest($orderno){
         $sql = "update TBL_SalsOrder_Ds t1, TBL_SOItemList_Ds t2 set 
			t1.status = -1, t2.status = -1 where t1.id =t2.f_orderid and t1.orderno = '$orderno';";
         $this->DB->update($sql);
    }
    
    //Read Sales Order From EDI File
    function importPurchaseOrder($filename ){

                $edi_content = file_get_contents( DROPSHIP_INBOX_URI.$filename );
			
                $parser = new PoEdiParser();
                $parsed = $parser->parse($edi_content);
                
                if($parsed){
                    if(DEBUG_MODE){
                        printLog($parsed);
                    }else{
                        self::insertSalesOrder($parser,$affected_soid, $isNewOrder);

                        if ( true || $isNewOrder == 1 ){
                            self::insertSalesOrderItem_helz($parser, $affected_soid);
                            echo "<br /><span style='color:green'>New Order & Item inserted -> OK</span>";
                        }else{
                            echo "<br /><span style='color:red'>".$parser->getItem('sonum')." Order already existed.</span>";  
                        }

                        //EDIFile::move_inbox_file_to_log($filename, "850" ,$parsed->sonum);
                    }
                    
		}
    }
    
    //Read sales order From Database
    function parsePurchaseOrder($sonum){
        
        $sql = "SELECT fcontent FROM egnymasterdb.tbl_edi_list WHERE orderno = '$sonum'";
        
        $result = $this->DB->get_query_data( $sql );
        $edi_content = $result[0]['fcontent'];
        $parser = new PoEdiParser();
        $parsed = $parser->parse($edi_content);

        if($parsed){
            if( DEBUG_MODE){
                printLog($parsed);
            }else{
                self::insertSalesOrder($parser,$affected_soid, $isNewOrder);

                if ( true || $isNewOrder == 1 ){
                    self::insertSalesOrderItem_helz($parser, $affected_soid);
                    echo "<br /><span style='color:green'>New Order & Item inserted -> OK</span>";
                }else{
                    echo "<br /><span style='color:red'>".$parser->getItem('sonum')." Order already existed.</span>";  
                }

                //EDIFile::move_inbox_file_to_log($filename, "850" ,$parsed->sonum);
            }

        }
    }
    
    //Read sales order From Database
    function parseWebOrderNo($sonum){
        
        $sql = "SELECT fcontent FROM egnymasterdb.tbl_edi_list WHERE orderno = '$sonum'";
        
        $result = $this->DB->get_query_data( $sql );
        $edi_content = $result[0]['fcontent'];
        $parser = new PoEdiParser();
        $parsed = $parser->parse($edi_content);

        if($parsed){
            if( DEBUG_MODE){
                printLog($parsed);
            }else{
                self::insertSalesOrder($parser,$affected_soid, $isNewOrder);

               

                //EDIFile::move_inbox_file_to_log($filename, "850" ,$parsed->sonum);
            }

        }
    }
    
    function insertSalesOrder($parser, &$affected_soid, &$isNewOrder){
                 
        $g = $parser->getCollection();
	
        $sl_ship_addr1 = addslashes($g['ship_addr1']);
        $sl_ship_city = addslashes($g['ship_city']);
        $sl_ship_custname = str_replace("'", "\'", $g['ship_custname']);
        
   
        $sql = "SELECT id FROM TBL_SalesOrder_Ds WHERE orderno = '$g[sonum]' LIMIT 1";
        $rows = $this->DB->get_query_data( $sql );
	$isNewOrder =  count($rows) == 0;
        
        if ( $isNewOrder ){ 
            $sql = "INSERT INTO TBL_SalesOrder_Ds "
                    . "(orderno,  weborderno, regdate,  shiptoid,  type,  status,  salestax,  statuschanged,  departmentid,  total,  lastchanged,  shippingtype,  docno,qbid, term, shipdate, aftercanceldate, requestshipdate, closed, groupno, shipcharge, shipcustname, shipaddr1, shipaddr2, shipaddr3, shipcity, shipstate, shipzip,shipcountry,phonenum,req_shipdate,req_canceldate,org_podate) "
                    . "VALUES "
                    . "('$g[sonum]', '$g[weborderno]',  now(),   1,   1,   0,   0,   null,   '$g[depid]',   0,   now(),   '$g[packmethod]', '$g[docno]', 'qbid', 'term', null,null, null, 0,'$g[grpno]', null,'$sl_ship_custname','$sl_ship_addr1','$g[ship_addr2]','$g[ship_addr3]','$sl_ship_city','$g[ship_state]','$g[ship_zip]','$g[shipcountry]','$g[phonenum]','$g[req_shipdate]','$g[req_canceldate]','$g[org_podate]');";

            $this->DB->update($sql);
            $affected_soid = mysql_insert_id();
        }else{
            $sql = "UPDATE TBL_SalesOrder_Ds SET "
                    . "weborderno = '$g[weborderno]',"
                    . "departmentid='$g[depid]',"
                    . "shippingtype = '$g[packmethod]',  "
                    . "docno='$g[docno]' ,"
                    . "groupno='$g[grpno]',"
                    . "shipcustname = '$sl_ship_custname', "
                    . "shipaddr1 = '$sl_ship_addr1', "
                    . "shipaddr2 = '$g[ship_addr2]', "
                    . "shipaddr3 = '$g[ship_addr3]', "
                    . "shipcity = '$sl_ship_city', "
                    . "shipstate = '$g[ship_state]', "
                    . "shipzip = '$g[ship_zip]', "
                    . "shipcountry = '$g[shipcountry]', "
                    . "phonenum = '$g[phonenum]', "
                    . "req_shipdate = '$g[req_shipdate]', "
                    . "req_canceldate = '$g[req_canceldate]', "
                    . "org_podate = '$g[org_podate]' "
                    . " WHERE orderno = '$g[sonum]'";
            
                    
            $this->DB->update($sql);
            
            $affected_soid = $rows[0]['id'];
        }
    }

    function insertSalesOrder_test($parser, &$affected_soid, &$isNewOrder){
                 
        $g = $parser->getCollection();
	
        $sl_ship_addr1 = addslashes($g['ship_addr1']);
        $sl_ship_city = addslashes($g['ship_city']);
        $sl_ship_custname = str_replace("'", "\'", $g['ship_custname']);
        
   
        $sql = "SELECT id FROM TBL_SalesOrder_Ds WHERE orderno = '$g[sonum]' LIMIT 1";
        $rows = $this->DB->get_query_data( $sql );
	$isNewOrder =  count($rows) == 0;
        
        if ( $isNewOrder ){ 
            $sql = "INSERT INTO TBL_SalesOrder_Ds "
                    . "(orderno,  weborderno, regdate,  shiptoid,  type,  status,  salestax,  statuschanged,  departmentid,  total,  lastchanged,  shippingtype,  docno,qbid, term, shipdate, aftercanceldate, requestshipdate, closed, groupno, shipcharge, shipcustname, shipaddr1, shipaddr2, shipaddr3, shipcity, shipstate, shipzip,shipcountry,phonenum,req_shipdate,req_canceldate,org_podate) "
                    . "VALUES "
                    . "('$g[sonum]', '$g[weborderno]',  now(),   1,   1,   0,   0,   null,   $g[depid],   0,   now(),   '$g[packmethod]', '$g[docno]', 'qbid', 'term', null,null, null, 0,'$g[grpno]', null,'$sl_ship_custname','$sl_ship_addr1','$g[ship_addr2]','$g[ship_addr3]','$sl_ship_city','$g[ship_state]','$g[ship_zip]','$g[shipcountry]','$g[phonenum]','$g[req_shipdate]','$g[req_canceldate]','$g[org_podate]');";

           
        }else{
            $sql = "UPDATE TBL_SalesOrder_Ds SET "
                    . "weborderno = $g[weborderno],"
                    . "regdate = now() ,"
                    . "departmentid=$g[depid],"
                    . "total = 0,"
                    . "lastchanged = now(),  "
                    . "shippingtype = $g[packmethod],  "
                    . "docno $g[docno] ,"
                    . "groupno=$g[grpno],"
                    . "shipcustname = $sl_ship_custname, "
                    . "shipaddr1 = $sl_ship_addr1, "
                    . "shipaddr2 = $g[ship_addr2], "
                    . "shipaddr3 = $g[ship_addr3], "
                    . "shipcity = $sl_ship_city, "
                    . "shipstate = $g[ship_state], "
                    . "shipzip = $g[ship_zip],"
                    . "shipcountry = $g[shipcountry],"
                    . "phonenum = $g[phonenum],"
                    . "req_shipdate = $g[req_shipdate],"
                    . "req_canceldate = $g[req_canceldate],"
                    . "org_podate = $g[org_podate] "
                    . " WHERE orderno = '$g[sonum]'";
                    
           
        }
        echo $sql;
    }
    
    function insertSalesOrderItem($parser,$affected_id){
        
        $g  = $parser->getCollection();
        $item = $this->getItemPrice($g);
        $price = $item[0]['IPPrice'];
        $f_itemid = $item[0]['IPid'];
        $f_mercode = $item[0]['IPBuyer'];
        $f_size = $item[0]['IPSize'];
        $f_vendorsku = $item[0]['IPBsku'];
        $f_itemcode = $arr_iteminfo[0]['IPBrefersku'];
        $cust_id = $this->getIPBuyerCode($g['merchant_id']);

        $sql = "INSERT INTO TBL_SOItemList_Ds (f_orderid, price, qty, f_itemid, status, shipeddate,trackno, seqno, size, f_sonum, f_vendorsku,f_itemcode, so_regdate, f_depnum,cust_str,ext1,cust_id, active) VALUES ( $affected_id, $g[price],  $g[unit_qty], $g[f_itemid], 1, now(),'', null, '$g[f_size]', '$g[sonum]', '$f_vendorsku','$f_itemcode',now(),'$f_mercode','$g[merchant_id]','$g[num]','$cust_id', 1 );";
        $this->DB->update($sql);

        $sql = "UPDATE TBL_SalesOrder_Ds SET total = total + $g[unit_qty] WHERE id = $soid;";
        $this->DB->update($sql);
    }


    function insertSalesOrderItem_helz($parser, $affected_id){
        
        $g = $parser->getCollection();
        $item = $this->getItemPrice($g);
        $f_itemid = $item[0]['IPid'];
        $f_mercode = $item[0]['IPBuyer'];
       
        $f_size = $item[0]['IPSize'];
        $f_vendorsku = $item[0]['IPBsku'];
        $f_itemcode = $item[0]['IPBrefersku'];
        
         echo "<br />".$g['itemcode']."<br />";
         
        $cust_id = $this->getIPBuyerCode($g['merchant_id']);     
        
        $sql = "INSERT INTO TBL_SOItemList_Ds (f_orderid, price, qty, f_itemid, status, shipeddate,trackno, seqno, size, f_sonum, f_vendorsku,f_itemcode, so_regdate, f_depnum,cust_str,ext1,cust_id, active ) VALUES ($affected_id, $g[itemprice],  $g[unit_qty], $f_itemid, 1, now(),'', null, '$f_size', '$g[sonum]', '$f_vendorsku','$f_itemcode',now(),'$f_mercode','$g[merchant_id]','$g[num]','$cust_id', 1 );";
        $this->DB->update($sql);
        
        $sql = "UPDATE TBL_SalesOrder_Ds SET total = total + $g[unit_qty] WHERE id = $affected_id;";
        $this->DB->update($sql);
    }

    public function updateShippingMethod($DB, $filepath ){

                $edi_content = file_get_contents( $filepath );
		
                $parser = new PoEdiParser();
                $parser->parse($edi_content);
                $g = $parser->getCollection();
                $sql = "UPDATE TBL_SalesOrder_Ds SET shippingtype = '$g[packmethod]' WHERE orderno='$g[sonum]'";
                $DB->update($sql);
                
    }
    
    private function getIPBuyerCode($f_merid){
            if ( $f_merid == "sears")
            return 'B10133';
            if ( $f_merid == "iqvc")
            return 'B20009';
            if ( $f_merid == "jcpenney")
            return 'B10007';
            if ( $f_merid == "helz" )
            return 'B10122';
    }

    private function getItemPrice($collection){
        
        $merchant_id = $collection['merchant_id'];
        $doc_no = $collection['docno'];
        $size = $collection['size'];
        $itemcode = $collection['itemcode'];
        $ipbuyercode = $this->getIPBuyerCode($merchant_id);
        
        $sql = "SELECT IPTSku,IPBsku, IPSize, IPBuyer, IPBrefersku,IPid,IPPrice,IPCost "
                . "FROM TBL_ItemPrice "
                . "WHERE IPBuyerType='B' "
                . "AND IPBuyer = '$ipbuyercode'";

        if ($merchant_id == "iqvc" || $merchant_id == "sears") {
            $sql = $sql. " AND IPTSku = '$itemcode'";
        }else if ($merchant_id == "jcpenney" ){
            $sql = $sql. " AND IPBsku = '$docno'";
        }else if ($merchant_id == "helz" ){
            if( substr($itemcode, 0, 1) == "R"){
                $size = intval($size);
                $sql = $sql. " AND IPBsku LIKE '$doc_no"."_".$size."%' LIMIT 1;";
            }else{
                $sql = $sql. " AND IPBsku LIKE '$doc_no%' LIMIT 1;";	
            }
        }

        return $this->DB->get_query_data( $sql );
	
    }
}