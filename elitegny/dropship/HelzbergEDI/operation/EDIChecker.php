<?php

namespace dropship\HelzbergEDI\operation;

//include_once( "./EDIHandler.php");
//include_once("./EDIFile.php");

use dropship\HelzbergEDI\operation\EDIHandler as EDIHandler;
use dropship\HelzbergEDI\operation\EDIFile;

class EDIChecker{
    
    protected static $filename ;
    protected static $ediHandler;
    public static function check($filename){
                
                self::$filename = $filename;
		
                $content = file_get_contents( DROPSHIP_INBOX_URI.$filename );
                
                 self::$ediHander = new EDIHandler();
                 
                 //self::$ediHandler->storeEDI($content,$filename);
		
                // functional receipt after sending 810 or 856 or 846
                if ( strpos($content , "*997*") !== false ){	
                    //echo "PROCESS 997";
                    self::process997($content);
                    
		//EDI 824 Application Advice:		
		}else if( strpos($content , "*824*") !== false ){
                    //echo "PROCESS 824";
                    self::process824($content);
			
		}else if ( strpos($content , "*860*") !== false ){
                    //echo "PROCESS 860";
                    self::process860($content);
	
                /* PURCHASE ORDER */
		}else if ( strpos($content , "*850*") !== false ){ 
                    //echo "PROCESS PO 850";
                   
                   self::process850($content);
		
		}
                
    }
   
    public static function read($sonum){
        
        $ediHander = new EDIHandler();
                    
        $ediHander->parsePurchaseOrder($sonum );
    }
    
     public static function readWeborder($sonum){
        
        $ediHander = new EDIHandler();
                    
        $ediHander->parseWebOrderNo($sonum );
    }
    
    //EDI 997 Functional Acknowledgment: 
    // => serves as a receipt, to acknowledge that an EDI transaction
    
    protected static function Process997($edi_str){
                        
                if ( strpos($edi_str , "*856*") !== false ){
                    self::process997_856($edi_str);
                }

                if ( strpos($edi_str , "*810*") !== false ){
                    self::process997_810($edi_str);

                }		
    }
    
    //EDI 856 Ship Notice/Manifest:
    
    protected static function process997_856($edi_str){
        
        $rows = explode( "*856*", $edi_str );
        $orderinfo = explode ( "~", $rows[1]);
        $orderno = $orderinfo[0];
        
        $v_fcate = "997_856";
        $v_memo = '997(856) - PO: '. $orderno . ' Ship-Notice has been received by '. "Helzberg";	
                        
        
        self::$ediHandler->addToEDIList(self::$filename,'',$edi_str,5,'helz','1',$v_memo,$v_fcate);
        self::$ediHandler->notifiedShipNoticeReception($orderno);
        
         EDIFile::import_997(self::$filename, $v_fcate, $orderno);
    }
    
    
    //EDI 997 -  FA Invoice RECEIPT:
    protected static function process997_810($edi_str){
        
        $rows = explode( "*810*", $edi_str );
        $orderinfo = explode ( "~", $rows[1]);
        $orderno = $orderinfo[0];
        
        $v_memo = '997(810) - PO: '.$orderno.' Invoice receipt has been received by '."Helzberg";			
	$v_fcate = "997_810";	
        
        self::$ediHandler->addToEDIList(self::$filename,'',$edi_str,5,'helz','1',$v_memo,$v_fcate);
        
        self::$ediHandler->notifiedInvoiceReception($orderno);
        
        EDIFile::import_997(self::$filename, $v_fcate, $orderno);
 
    }
    
    //EDI 824 Application Advice:
    //The X12 824 transaction set is the electronic version of an Application Advice document, 
    //used to notify the sender of a previous transaction that the document has been accepted, or to report on error
    
    protected static function Process824($edi_str){
        
        
                        //TED -- Technical Error Description 
                        $arr = explode($edi_str,"~TED*");
                        $arrr = explode($arr[1],"*");
                   
                        $v_tedcode = $arrr[0];
                        $v_msg = $arrr[1];
                        
                        $arr =  explode($edi_str, "*VN*");
			$arrr = explode( $arr[1], "~"); 
			
                        $orderno = $arrr[0]; 
			
                        //OTI LOOP-- Original Transaction Identification						
                        
                        $brr = explode($edi_str,"OTI*");
                        $brrr = explode($brr[1],"*");
                        $v_invno_result = $brrr[0];//	-- TA : Accepted	
                        $v_invno = $brrr[2];	//-- INV NO
                        $v_orderid = $brrr[8];//-- orderid 
                        $$v_fcate = '824';
			
                        switch ($v_invno_result){
                            case "TA" : $memo_txt = ' has been accepted by ';
                                        self::$ediHandler->notifiedInvoiceAccepted($orderno);
                                        break;
                            case "TC" : $memo_txt = ' ( w/ data change ) has been accepted by ';
                                        break;
                            case "TR" : $memo_txt = ' has been rejected by ';	
                                        self::$ediHandler->notifiedInvoiceRejected($orderno);
                                        break;
			}	
			
			
			$v_memo =   'P.O Invoice - HZN'. $v_orderid. $memo_txt.  $v_cust_name . ' [ MSG:' . $v_invno_result . '_'. $v_tedcode .' =>' . $v_msg .']';
                       
                        self::$ediHandler->addToEDIList(self::$filename,'',$edi_str,5,'helz','1',$v_memo,$v_fcate);
                       
                        EDIFile::import(self::$filename, "824");				
    }
			
    
    //EDI 860 Purchase Order Change Request
    
    protected static function Process860($edi_str){
        
        $rows = explode( "BCH*", $edi_str );
        $orderinfo = explode ( "*", $rows[1]);
        $orderno = $orderinfo[2];
        $v_fcate = "860";
        $v_memo = 'P.O Information / Status Changed By ' . "Helzberg";	
        
        self::$ediHandler->addToEDIList(self::$filename,'',$edi_str,5,'helz','1',$v_memo,$v_fcate);
        self::$ediHandler->orderChangeRequest($orderno);
        EDIFile::import_860(self::$filename, "860", $orderno);
	
    }	
    
     protected static function Process850($edi_str){
        
        $v_memo = 'New P.O Received By '."Helzberg";	
        $v_fcate = '850';
        
        self::$ediHandler->addToEDIList(self::$filename,'',$edi_str,5,'helz','1',$v_memo,$v_fcate);
         
        self::$ediHander->importPurchaseOrder($filename );
	
    }	

    
}