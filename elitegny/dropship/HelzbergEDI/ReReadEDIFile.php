<?php
//error_reporting(E_ALL);

namespace dropship;


include("Configuration.php");
include_once("operation/EDIChecker.php");
include_once("operation/EDIHandler.php");

include_once("../Nintra/Database/NintraDB.php");
include_once("PurchaseOrder/PoEdiParser.php");

use dropship\HelzbergEDI\operation\EDIHandler;
use dropship\Nintra\Database\NintraDB AS NintraDB;

//copy to new folder
$files = scandir("../inbox_2017_2_7");

$nintra = new NintraDB();

foreach($files as $row){
    echo $row."<br />";
    if ( strpos( $row, "{" ) !== false ){
           
           EDIHandler::updateShippingMethod($nintra,"../inbox_2017_2_7/".$row);
            
    }
}

$nintra->close();
