<?php
namespace dropship\HelzbergEDI\ShipNotice;
//include_once("../Nintra/Database/NintraDB.php");
//include_once("operation/EDIFile.php");

use dropship\Nintra\Database\NintraDB;
use dropship\HelzbergEDI\operation\EDIFile;

class Exporter{
    
    static $DB;
    
    static function generate($sdate = ""){
      
        if ( !isset($sdate) || $sdate == "" ){ 
                $sdate = date('Y-m-d', time()-824000); // 9.5 days ago
        }
        self::$DB = new NintraDB();

        // MAKE SHIPNOTICE FILES w/ only the PO(s) not generated for shipnotice.
        if(DEBUG_MODE){
        $sql = "SELECT t1.*, t2.`f_itemcode`, t2.`f_vendorsku`, tp.IPDesc "
                . "FROM TBL_SalesOrder_Ds t1, TBL_SOItemList_Ds t2, TBL_ItemPrice tp "
                . "WHERE t1.id = t2.`f_orderid` "
                . "AND  t2.f_itemid = tp.IPid "
                . "AND t1.departmentid = 5 "
                . "AND t1.status = 4 and t2.trackno != '' "
                . "AND (t1.shipnoticed < 1 OR t1.ship_confirmed <> 'Y') "
                . "AND t1.statuschanged  >= '$sdate' "
                . "AND t1.groupno IS NOT NULL ORDER BY t1.id DESC;";
        
        }else{
        $sql = "SELECT t1.*, t2.`f_itemcode`, t2.`f_vendorsku`, tp.IPDesc "
                . "FROM TBL_SalesOrder_Ds t1, TBL_SOItemList_Ds t2, TBL_ItemPrice tp "
                . "WHERE t1.id = t2.`f_orderid` "
                . "AND  t2.f_itemid = tp.IPid "
                . "AND t1.departmentid = 5 "
                . "AND t1.status = 4 and t2.trackno != '' "
               /* . "AND t1.statuschanged  >= '$sdate' "*/
                . "AND t1.lastchanged  >= '$sdate' "
                . "AND t1.groupno IS NOT NULL ORDER BY t1.id DESC;";
        }
        
        $out_arr_info = self::$DB->get_query_data( $sql );
        $list_orderno = "";
        
        foreach ( $out_arr_info as $row ){
            //$sdate = date("Ymd", $row[regdate]);
            $orderid = $row['id'];
            $orderno = $row['orderno'];
            $shipdate = date('Ymd', strtotime($row['org_podate']));
            $short_shipdate = substr(date('Ymd', strtotime($row['org_podate'])), 2, 6);
            $org_podate = date('Ymd', strtotime($row['org_podate']));
            $trackno = $row['trackno'];
            $grpno = $row['groupno'];
            $orderid_ninedigits = self::getStrWithDigits ( $orderid, 9 );
            $shipcustname = $row['shipcustname'];
            $shippingtype = self::filterShippingType($row['shippingtype']);
            $item_desc = $row['IPDesc'];;
            $itemcode = $row['f_itemcode'];
            $arr_vendorsku = explode("_", $row['f_vendorsku']);
            $vendorsku = $arr_vendorsku[0];
            $shipaddr1 = $row['shipaddr1'];
            $shipcity = $row['shipcity'];
            $shipstate = $row['shipstate'];
            $shipzip = $row['shipzip'];
            $shiptime = date("His", time());
            
            $msg = "ISA*00*          *00*          *01*LXGM           *01*006965735      *$short_shipdate*1805*U*00403*$orderid_ninedigits*0*P*|";
            $msg .= "~GS*SH*LXGM*00*$org_podate*1805*$grpno*X*004030";
            $msg .= "~ST*856*$orderid";
            $msg .= "~BSN*00*$orderid*$shipdate*1805";
            $msg .= "~HL*1**S";
            $msg .= "~TD1*CTN*1****G*1*LB";
            $msg .= "~TD5*O*2*$shippingtype*U*$shippingtype";
            $msg .= "~REF*CN*$trackno";
            $msg .= "~PER*BD*Jin*WP*2127255049*EM*jin.m@elitegny.com";
            $msg .= "~DTM*011*$shipdate*";
            $msg .= "~N1*ST*$shipcustname*92*0000";
            $msg .= "~N3*$shipaddr1";
            $msg .= "~N4*$shipcity*$shipstate*$shipzip*";
            $msg .= "~HL*2*1*O";
            $msg .= "~PRF*$orderno***$shipdate*";
            $msg .= "~HL*3*2*I";
            $msg .= "~LIN**IN*$vendorsku*VN*$itemcode";
            $msg .= "~SN1**1*EA**1*EA";
            $msg .= "~PID*F*08***$item_desc";
            $msg .= "~CTT*1*";
            $msg .= "~SE*19*$orderid";
            $msg .= "~GE*1*$grpno";
            $msg .= "~IEA*1*$orderid_ninedigits~";

            $list_orderno .= "'$orderno',";
            $filename =  "856_".$orderno."_".$shiptime.".edi";
            echo $msg;echo "<br />###############<br />";
            EDIFile::writeShipNotice($msg,$filename );

        }
        
        if ( $list_orderno != "" ){
           $list_orderno = substr($list_orderno, 0, -1); //remove comma
         }
        return $list_orderno;       
    }
    
    static function generateBySO($sdate,$ids){
        
        if ( !isset($sdate) || $sdate == "" ){ 
                $sdate = date('Y-m-d', time()-824000); // 9.5 days ago
        }
        self::$DB = new NintraDB();

        // MAKE SHIPNOTICE FILES w/ only the PO(s) not generated for shipnotice.
       
        $sql = "SELECT t1.*, t2.`f_itemcode`, t2.`f_vendorsku`, tp.IPDesc "
                . "FROM TBL_SalesOrder_Ds t1, TBL_SOItemList_Ds t2, TBL_ItemPrice tp "
                . "WHERE t1.id = t2.`f_orderid` "
                . "AND t1.orderno in (".$ids.") " 
                . "AND  t2.f_itemid = tp.IPid "
                . "AND t1.departmentid = 5 "
                . "AND t2.trackno != '' "
                . "AND t1.groupno IS NOT NULL ORDER BY t1.id DESC;";
      
  $sql = "SELECT distinct t1.groupno,t1.*, t2.`f_itemcode`, t2.`f_vendorsku` "
                . "FROM TBL_SalesOrder_Ds t1, TBL_SOItemList_Ds t2 "
                . "WHERE t1.id = t2.`f_orderid` "
                . "AND t1.orderno in (".$ids.") " 
                . "AND t1.departmentid = 5 "
                . "AND t1.trackno != '' "
                . "AND t1.groupno IS NOT NULL ORDER BY t1.id DESC;";
  
  echo $sql;
        $out_arr_info = self::$DB->get_query_data( $sql );
        $list_orderno = "";
      
        foreach ( $out_arr_info as $row ){
            //$sdate = date("Ymd", $row[regdate]);
           
            $orderid = $row['id'];
            $orderno = $row['orderno'];
            $shipdate = date('Ymd', strtotime($row['org_podate']));
            $short_shipdate = substr(date('Ymd', strtotime($row['org_podate'])), 2, 6);
            $org_podate = date('Ymd', strtotime($row['org_podate']));
            $trackno = $row['trackno'];
            $grpno = $row['groupno'];
            $orderid_ninedigits = self::getStrWithDigits ( $orderid, 9 );
            $shipcustname = $row['shipcustname'];
            $shippingtype = self::filterShippingType($row['shippingtype']);
            $item_desc = isset($row['IPDesc'])?$row['IPDesc']:"...";
            $itemcode = $row['f_itemcode'];
            $arr_vendorsku = explode("_", $row['f_vendorsku']);
            $vendorsku = $arr_vendorsku[0];
            $shipaddr1 = $row['shipaddr1'];
            $shipcity = $row['shipcity'];
            $shipstate = $row['shipstate'];
            $shipzip = $row['shipzip'];
            $shiptime = date("His", time());
            
            $msg = "ISA*00*          *00*          *01*LXGM           *01*006965735      *$short_shipdate*1805*U*00403*$orderid_ninedigits*0*P*|";
            $msg .= "~GS*SH*LXGM*00*$org_podate*1805*$grpno*X*004030";
            $msg .= "~ST*856*$orderid";
            $msg .= "~BSN*00*$orderid*$shipdate*1805";
            $msg .= "~HL*1**S";
            $msg .= "~TD1*CTN*1****G*1*LB";
            $msg .= "~TD5*O*2*$shippingtype*U*$shippingtype";
            $msg .= "~REF*CN*$trackno";
            $msg .= "~PER*BD*Jin*WP*2127255049*EM*jin.m@elitegny.com";
            $msg .= "~DTM*011*$shipdate*";
            $msg .= "~N1*ST*$shipcustname*92*0000";
            $msg .= "~N3*$shipaddr1";
            $msg .= "~N4*$shipcity*$shipstate*$shipzip*";
            $msg .= "~HL*2*1*O";
            $msg .= "~PRF*$orderno***$shipdate*";
            $msg .= "~HL*3*2*I";
            $msg .= "~LIN**IN*$vendorsku*VN*$itemcode";
            $msg .= "~SN1**1*EA**1*EA";
            $msg .= "~PID*F*08***".self::truncate($item_desc,80);
            $msg .= "~CTT*1*";
            $msg .= "~SE*19*$orderid";
            $msg .= "~GE*1*$grpno";
            $msg .= "~IEA*1*$orderid_ninedigits~";

            $list_orderno .= "'$orderno',";
            $filename =  "856_".$orderno."_".$shiptime.".edi";
            echo "[trackno]".$trackno."  [filename]".$filename;
            echo "<br />".$msg;
            echo "<br />################";
            EDIFile::writeShipNotice($msg,$filename );

        }
        
        if ( $list_orderno != "" ){
           $list_orderno = substr($list_orderno, 0, -1); //remove comma
         }
        return $list_orderno;       
    }

    static function updateStatus($ids){
       
        if ( $ids != "" ){
            $sql = "UPDATE TBL_SalesOrder_Ds SET shipnoticed = 1 WHERE departmentid =5 AND orderno in ( $ids );";
            if(DEBUG_MODE){
                printLog($sql,LOG_TYPE_WARN);
            }else{
                self::$DB->update( $sql );
            }
        }
    }
    
    static function  truncate($string, $length, $dots = "...") {
        return (strlen($string) > $length) ? substr($string, 0, $length - strlen($dots)) . $dots : $string;
    }

    static function getStrWithDigits ( $num, $totalcnt ){

           $len = strlen($num);
           $first_zero_strcnt = $totalcnt - $len;
           $i = 1;
           $first_zero = "";
           for ( $i = 1;$i<=$first_zero_strcnt; $i++ ){

                   $first_zero .= "0";
           }

           return $first_zero.$num;
   }
   
    static function filterShippingType($method){
        if($method == "UPSN_CG") { return "FDEG";}
        else if($method == "UPSN") { return "FDEG";}
        else if($method == "UPS2") { return "FDE2";}
        else if($method == "UPSG") { return "FDEG"; }
        else if($method == "UPSN_ND") { return "FDE1";}
        else if($method == "UPSN_2D") { return "FDE2";}
        else {       return $method;     }
    //Use FDEG for ground, FDE1 for overnight, and use FDE2 for second day.  
    }

}
