<?php
//error_reporting(E_ALL);
namespace dropship\HelzbergEDI;

include("Configuration.php");
//include("../__autoload.php");
include("./operation/EDIChecker.php");
include_once("./operation/EDIFile.php");
include_once("./operation/EDIHandler.php");
include_once("./Inventory/HelzInventoryHandler.php");
include_once("./PurchaseOrder/PoEdiParser.php");

use dropship\HelzbergEDI\operation\EDIChecker as EDIChecker;

$filename = $_GET['filename'];

if ( $filename != "") {
    EDIChecker::check( $filename );
}


?>