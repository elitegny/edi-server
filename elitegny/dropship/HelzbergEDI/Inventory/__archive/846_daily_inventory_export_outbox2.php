<?php
include("../class/mysql.class");
include("util.php");
function getIPSize( $ipbrefersku ){
	$str_size = trim( substr(trim($ipbrefersku), 13, 3) );

	if ( $str_size == "010" )
		return "10";
 	if ( $str_size == "100" )
		return "10";	
	if ( $str_size == "040" )
		return "4";		
	if ( $str_size == "050" )
		return "5";
	if ( $str_size == "060" )
		return "6";
	if ( $str_size == "070" )
		return "7";
	if ( $str_size == "080" )
		return "8";
	if ( $str_size == "090" )
		return "9";
	if ( $str_size == "017" )
		return "17";
	if ( $str_size == "000" )
		return "0";		
		
	return "725";
}


function getStrWithDigits ( $num, $totalcnt ){
	
	$len = strlen($num);
	$first_zero_strcnt = $totalcnt - $len;
	$i = 1;
	$first_zero = "";
	for ( $i = 1;$i<=$first_zero_strcnt; $i++ ){
	
		$first_zero .= "0";
	}

	return $first_zero.$num;
}
$sdate = $_GET['sdate']; 
//$sdate = "2013-12-19";

if ( !isset($sdate) || $sdate == "" ){ 
	$sdate = date('Y-m-d', time()); // since date
}


//$db = new redDB("intra.elitegny.com", "root", "Elitegny11!", "EGNYMasterDB");
$db = new redDB("nintra.elitegny.com", "elitegny", "elitegny", "egnymasterdb");

$sql = "SELECT t1.*, t2.`f_itemcode`, t2.`f_vendorsku`, tp.IPDesc, tp.IPStock, tp.IPPrice, tp.IPSize FROM 
TBL_SalesOrder_Ds t1, 
TBL_SOItemList_Ds t2, 
TBL_ItemPrice tp WHERE t1.id = t2.`f_orderid` AND
t2.f_itemid = tp.IPid AND 
t1.departmentid = 5 AND t1.status = 4 
AND t1.regdate <= (SELECT LEFT(regdate, 10) FROM TBL_SalesOrder_Ds WHERE departmentid = 5 AND STATUS = 4 AND regdate < '2013-12-25' ORDER BY regdate DESC LIMIT 1 )
 ORDER BY t1.id DESC;";
$sql =  "SELECT t1.*, record.*, tp.IPDesc, tp.IPStock, tp.IPPrice, tp.IPSize  FROM TBL_SalesOrder_Ds t1, 
 (  
    SELECT DISTINCT t.f_itemcode, t.f_vendorsku, t.f_itemid, 
	(SELECT f_orderid FROM TBL_SOItemList_Ds WHERE STATUS = 4 AND f_itemcode =  t.f_itemcode ORDER BY id DESC LIMIT 1 ) AS orderid

  FROM TBL_SOItemList_Ds t WHERE t.cust_str = 'helz' AND t.status = 4
  ) record, TBL_ItemPrice tp WHERE t1.id = record.orderid AND 
   record.f_itemid = tp.IPid";

$sql =  "SELECT t1.*, t2.* FROM TBL_SalesOrder_Ds t1, TBL_SOItemList_Ds t2 
where t1.id = t2.f_orderid and t1.departmentid = 5 and t1.status = 4 order by t1.regdate desc limit 1;";
   
$out_arr_info = $db->get_query_data( $sql );
//print_r($out_arr_info);
$list_orderno = "";
$msg = "";
foreach ( $out_arr_info as $arr_info ){
		
		//$sdate = date("Ymd", $arr_info[regdate]);
		
		$orderid = $arr_info[id];
		$orderno = $arr_info[orderno];
		$shipdate = date('Ymd', strtotime($arr_info[regdate]));
		$short_shipdate = substr(date('Ymd', strtotime($arr_info[regdate])), 2, 6);	
		$trackno = $arr_info[trackno];
		$grpno = $arr_info[groupno];
		$orderid_ninedigits = getStrWithDigits ( $orderid, 9 );		
		$shipcustname = $arr_info[shipcustname];
		$shippingtype = $arr_info[shippingtype];
	//	$item_desc = substr(TRIM($arr_info[IPDesc]),0,40)."..";
		$itemcode = $arr_info[f_itemcode];
		
		$arr_vendorsku = explode("_", $arr_info[f_vendorsku]);
	//	$vendorsku = $arr_vendorsku[0];
	//	$item_qty = $arr_info[IPStock];
	//	$item_price = $arr_info[IPPrice];
	//	$item_size = intval($arr_info[IPSize]);
		
		$shipaddr1 = $arr_info[shipaddr1];
		$shipcity = $arr_info[shipcity];
		$shipstate = $arr_info[shipstate];
		$shipzip = $arr_info[shipzip];

		$shiptime = date("His", time());
		
		if ( $grpno != "" ){
$msg .= "ISA*00*          *00*          *01*LXGM           *01*006965735      *$short_shipdate*1202*U*00403*$orderid_ninedigits*0*T*|~";
$msg .= "GS*IN*LXGM*006965735*$shipdate*1202*$grpno*X*004030~";
$msg .= "ST*846*$orderid~";
$msg .= "BIA*00*ZZ*$itemcode*$shipdate*1423~";
$msg .= "N1*SU*LUXGEM*01*LXGM~";
$msg .= "N3*15 West 37th Street~";
$msg .= "N4*NEWYORK*NY*10018~";
$msg .= "PER*IC*JIN*WP*2127255049*EM*jin.m@elitegny.com~";

$msg .= "[INVENTORY_CONTENT]";

$msg .= "CTT*[INVENTORY_CNT]~";
$msg .= "SE*[SEGMENTS_CNT]*$orderid~";
$msg .= "IEA*1*$orderid_ninedigits~";	
$msg .= "\r\n";
				BREAK;
		}
		
		/*
$msg = "ISA*00*          *00*          *01*LXGM           *01*006965735      *$short_shipdate*1805*U*00403*$orderid_ninedigits*0*P*|";
$msg .= "~GS*SH*LXGM*00*$shipdate*1805*$grpno*X*004030";
$msg .= "~ST*856*$orderid";
$msg .= "~BSN*00*$orderid*$shipdate*1805";
$msg .= "~HL*1**S";
$msg .= "~TD1*CTN*1****G*1*LB";
$msg .= "~TD5*O*2*$shippingtype*U*$shippingtype";
$msg .= "~REF*CN*$trackno";
$msg .= "~PER*BD*Jin*WP*2127255049*EM*jin.m@elitegny.com";
$msg .= "~DTM*011*$shipdate*";
$msg .= "~N1*ST*$shipcustname*92*0000";
$msg .= "~N3*$shipaddr1";
$msg .= "~N4*$shipcity*$shipstate*$shipzip*";
$msg .= "~HL*2*1*O";
$msg .= "~PRF*$orderno***$shipdate*";
$msg .= "~HL*3*2*I";
$msg .= "~LIN**IN*$vendorsku*VN*$itemcode";
$msg .= "~SN1**1*EA**1*EA";
$msg .= "~PID*F*08***$item_desc";
$msg .= "~CTT*1*";
$msg .= "~SE*19*$orderid";
$msg .= "~GE*1*$grpno";
$msg .= "~IEA*1*$orderid_ninedigits~";
*/
		
		$list_orderno .= "'$orderno',";



echo "<hr />";
}

$sql2 =  "SELECT p1.*, 
(
 SELECT COUNT(DISTINCT p2.IPBuyer) FROM TBL_ItemPrice p2 WHERE
 p2.IPStatus = 'A' AND  p2.IPBrefersku = p1.IPBrefersku
 ) AS cnt_cust
from TBL_ItemPrice p1 where p1.IPBuyer = 'B10122' and p1.IPStatus = 'A';";
   
//echo $sql2;   

//   exit;
$out_arr_info2 = $db->get_query_data( $sql2 );
//print_R($out_arr_info2);
//exit;
$inv_cnt = 0;
$inventory_content ="";
$segment_cnt = 8;

foreach ( $out_arr_info2 as $arr_info ){
		
		//$sdate = date("Ymd", $arr_info[regdate]);

		$item_desc = trim( substr(TRIM($arr_info[IPDesc]),0,80));
		$itemcode = $arr_info[IPBrefersku];
		
		$arr_vendorsku = explode("_", $arr_info[IPBsku]);
		$vendorsku = $arr_vendorsku[0];
		$item_qty = $arr_info[IPStock];

/////////////////  

/*
        $tot_expected_qty = intval($item_qty/$arr_info['cnt_cust']);
        if ( $tot_expected_qty > 5){
                $min_qty = 5;
        }else{
                $min_qty = $tot_expected_qty;
        }
        if ( $arr_info['IPSpecialStock'] > 0 ){

//        $min_qty = $min_qty + ($arr_info['IPSpecialStock'] - $arr_info['IPSpecialSales']);
		$min_qty = $arr_info['IPSpecialStock'];
        }
*/
		
		$min_qty = $arr_info['IPCustStock'];
		if ( $min_qty < 0 ){
			$min_qty = 0;
		}
//////////////////		
		
		
		$item_price = $arr_info[IPPrice];

		$item_size = getIPSize(  $arr_info[IPBrefersku] );
	//		$item_size = intval($arr_info[IPSize]);
	//	$shiptime = date("His", time());
		$cm_term = ""; // "CM" IF COLOR EXIST
		$cm_info = ""; // "Yellow" - Color Info 
		$sz_term = "SZ"; // "SZ" if size exists
		$sz_info = $item_size;
//		if ( $grpno != "" ){
$inventory_content .= "LIN**IN*$vendorsku*VN*$itemcode*$cm_term*$cm_info*$sz_term*$item_size~";
$inventory_content .= "PID*F****$item_desc~";
$inventory_content .= "CTP**WHL*$item_price~";
$inventory_content .= "QTY*V2*$min_qty*EA~";

 $inv_cnt++;
 $segment_cnt = $segment_cnt + 4;
//		}
}
$msg = str_replace("[INVENTORY_CONTENT]", $inventory_content, $msg);
$msg = str_replace("[INVENTORY_CNT]", $inv_cnt, $msg);
$msg = str_replace("[SEGMENTS_CNT]", $segment_cnt, $msg);


$filename = "outbox_log/846_inventory_$sdate.edi";		
//$filename = "outbox_log/846_".$orderno."_".$shiptime.".edi";
//$filepath = realpath(getcwd(). "\\" . $filename);
$filepath = $filename;
//echo $msg;

ini_set('display_errors', 'On');
error_reporting(E_ALL);

_file_write ( $filepath, $msg );

$sql = "";
/*
insert into TBL_EDI_List 
(org_fname, fname, fcontent, regdate, cust_no, cust_name, orderno) 
values ( );";
$db->update($sql);

*/

/*
if ( $list_orderno != "" ){
$list_orderno = substr($list_orderno, 0, -1);
$sql = "update TBL_SalesOrder_Ds set shipnoticed = 1 where departmentid =5 and orderno in ( $list_orderno );";
$db->update( $sql );

}
*/
//echo "1"; // done; >> changed to 1







?>