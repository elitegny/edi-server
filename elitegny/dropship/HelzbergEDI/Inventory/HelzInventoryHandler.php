<?php

namespace dropship\HelzbergEDI\Inventory;
include_once("../Nintra/Database/NintraDB.php");
use dropship\Nintra\Database\NintraDB;

class HelzInventoryHandler{

    static $DB;
    
    public function __construct(){}
    
    public static function export($sdate){
       
        self::$DB = new NintraDB();
        $edidocument =  self::generateEDIDocument();
        self::$DB->close();
        return $edidocument;  
    }
    
    public static function generateEDIDocument(){
        
        $sql =  "SELECT t1.*, t2.* FROM TBL_SalesOrder_Ds t1, TBL_SOItemList_Ds t2 "
                . "WHERE t1.id = t2.f_orderid "
                    . "AND t1.departmentid = 5 "
                    . "AND t1.status = 4 "
                . "ORDER BY t1.regdate desc limit 1;";
   
        $result = self::$DB->get_query_data( $sql );
       
        $list_orderno = "";
        $msg = "";
       
        foreach ( $result as $row ){

            $orderid = $row['id'];
            $orderno = $row['orderno'];
            $shipdate = date('Ymd', strtotime($row['regdate']));
            $short_shipdate = substr(date('Ymd', strtotime($row['regdate'])), 2, 6);	
            $trackno = $row['trackno'];
            $grpno = $row['groupno'];
            $orderid_ninedigits = self::getStrWithDigits ( $orderid, 9 );		
            $shipcustname = $row['shipcustname'];
            $shippingtype = $row['shippingtype'];
            //	$item_desc = substr(TRIM($row[IPDesc]),0,40)."..";
            $itemcode = $row['f_itemcode'];

            $arr_vendorsku = explode("_", $row['f_vendorsku']);
            //	$vendorsku = $arr_vendorsku[0];
            //	$item_qty = $row[IPStock];
            //	$item_price = $row[IPPrice];
            //	$item_size = intval($row[IPSize]);

            $shipaddr1 = $row['shipaddr1'];
            $shipcity = $row['shipcity'];
            $shipstate = $row['shipstate'];
            $shipzip = $row['shipzip'];

            $shiptime = date("His", time());

            if ( $grpno != "" ){
                $msg .= "ISA*00*          *00*          *01*LXGM           *01*006965735      *$short_shipdate*1202*U*00403*$orderid_ninedigits*0*T*|~";
                $msg .= "GS*IN*LXGM*006965735*$shipdate*1202*$grpno*X*004030~";
                $msg .= "ST*846*$orderid~";
                $msg .= "BIA*00*ZZ*$itemcode*$shipdate*1423~";
                $msg .= "N1*SU*LUXGEM*01*LXGM~";
                $msg .= "N3*15 West 37th Street~";
                $msg .= "N4*NEWYORK*NY*10018~";
                $msg .= "PER*IC*JIN*WP*2127255049*EM*jin.m@elitegny.com~";
                $msg .= "[INVENTORY_CONTENT]";
                $msg .= "CTT*[INVENTORY_CNT]~";
                $msg .= "SE*[SEGMENTS_CNT]*$orderid~";
                $msg .= "IEA*1*$orderid_ninedigits~";	
                $msg .= "\r\n";
                BREAK;
            }

            $list_orderno .= "'$orderno',";
        }

        $sql2 =  "SELECT p1.*, ( SELECT COUNT(DISTINCT p2.IPBuyer) "
                            . "FROM TBL_ItemPrice p2 "
                            . "WHERE p2.IPStatus = 'A' AND  p2.IPBrefersku = p1.IPBrefersku) AS cnt_cust "
                . "FROM TBL_ItemPrice p1 "
                . "WHERE p1.IPBuyer = 'B10122' AND p1.IPStatus = 'A';";


        $result2 = self::$DB->get_query_data( $sql2 );
        
        $inv_cnt = 0;
        $inventory_content ="";
        $segment_cnt = 8;

        foreach ( $result2 as $row ){
		
            $item_desc = trim( substr(TRIM($row[IPDesc]),0,80));
            $itemcode = $row[IPBrefersku];

            $arr_vendorsku = explode("_", $row[IPBsku]);
            $vendorsku = $arr_vendorsku[0];
            $item_qty = $row['IPStock'];
            $min_qty = $row['IPCustStock'];
            if ( $min_qty < 0 ){
                    $min_qty = 0;
            }		
            $item_price = $row['IPPrice'];

            $item_size = self::getIPSize(  $row['IPBrefersku'] );
            $cm_term = ""; // "CM" IF COLOR EXIST
            $cm_info = ""; // "Yellow" - Color Info 
            $sz_term = "SZ"; // "SZ" if size exists
            $sz_info = $item_size;
            //		if ( $grpno != "" ){
            $inventory_content .= "LIN**IN*$vendorsku*VN*$itemcode*$cm_term*$cm_info*$sz_term*$item_size~";
            $inventory_content .= "PID*F****$item_desc~";
            $inventory_content .= "CTP**WHL*$item_price~";
            $inventory_content .= "QTY*V2*$min_qty*EA~";

             $inv_cnt++;
             $segment_cnt = $segment_cnt + 4;
        }
        $msg = str_replace("[INVENTORY_CONTENT]", $inventory_content, $msg);
        $msg = str_replace("[INVENTORY_CNT]", $inv_cnt, $msg);
        $msg = str_replace("[SEGMENTS_CNT]", $segment_cnt, $msg);

        return $msg;
    }
    
    public static function getIPSize( $ipbrefersku ){
	$str_size = trim( substr(trim($ipbrefersku), 13, 3) );

	if ( $str_size == "010" )
		return "10";
 	if ( $str_size == "100" )
		return "10";	
	if ( $str_size == "040" )
		return "4";		
	if ( $str_size == "050" )
		return "5";
	if ( $str_size == "060" )
		return "6";
	if ( $str_size == "070" )
		return "7";
	if ( $str_size == "080" )
		return "8";
	if ( $str_size == "090" )
		return "9";
	if ( $str_size == "017" )
		return "17";
	if ( $str_size == "000" )
		return "0";		
		
	return "725";
    }
    
    public static function getStrWithDigits ( $num, $totalcnt ){
	
	$len = strlen($num);
	$first_zero_strcnt = $totalcnt - $len;
	$i = 1;
	$first_zero = "";
	for ( $i = 1;$i<=$first_zero_strcnt; $i++ ){
	
		$first_zero .= "0";
	}

	return $first_zero.$num;
    }
}














?>