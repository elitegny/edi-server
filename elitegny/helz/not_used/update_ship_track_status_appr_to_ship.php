<?php
include ("../class/mysql.class");

function objectToArray($d) {
		if (is_object($d)) {
			// Gets the properties of the given object
			// with get_object_vars function
			$d = get_object_vars($d);
		}
 
		if (is_array($d)) {
			/*
			* Return array converted to object
			* Using __FUNCTION__ (Magic constant)
			* for recursive call 
			*/
			return array_map(__FUNCTION__, $d);
		}
		else {
			// Return array
			return $d;
		}
}
function fetchUrl($url) {
/*
        if ($this->config['retrMethod'] == 'curl') {
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $html = curl_exec($ch);
            curl_close($ch);

            if (function_exists('utf8_decode')) {
                $html = utf8_decode($html);
            }
        } else {
*/		
            $html = file_get_contents($url);
 //       }

        return $html;
}
function fedexTrack($trackingNumber) {
        $link = 'http://www.fedex.com/Tracking/Detail?template_type=detail&trackNum=' . $trackingNumber;
        $html = fetchUrl($link);

        if (!preg_match("#var detailInfoObject = ([^\n]+);\n#", $html, $jsonString)) {
            // Javascript-based package details object not found
            return false;
        }

        if (($data = json_decode($jsonString[1])) == false) {
            // Unable to decode expected JSON string
            return false;
        }

        // DEBUG
		$arr_data = objectToArray($data);
		
        $stats = array();
        $locations = array();

        // Gather shipment statistics
        $stats = array(
            'status' => $data->status,
            'destination' => $data->deliveryLocation,
            'service' => $data->serviceType,
            'details' => $data->weight,
            'departure' => $data->shipDate,
            'arrival' => $data->delivered,
            'est_arrival' => $data->estimatedDeliveryDate
        );

        // Gather details for each scan along the route
        foreach ($data->scans as $scan) {
            $row = array(
                'status' => $scan->scanStatus,
                'time' => $scan->scanDate . ' ' . $scan->scanTime . ' ' . $scan->GMTOffset,
                'location' => $scan->scanLocation,
                'details' => ($scan->showReturnToShipper) ? 'Show return to shipper: ' . $scan->showReturnToShipper : ''
            );

            $locations[] = $row;
        }

        if (count($locations)) {
            // Set the last location
            $stats['last_location'] = $locations[0]['location'];

            // Set the delivery date if applicable
            if ($stats['arrival']) {
                $stats['arrival'] = $locations[0]['time'];
            }
        }
		$stats['pono'] = $arr_data[altRefList][1][value];

        return array(
            'details' => $stats,
            'locations' => $locations
        );
}


function upsTrack($trackingNumber) {
	$data ="<?xml version=\"1.0\"?>
			<AccessRequest xml:lang='en-US'>
					<AccessLicenseNumber>8BEFB9015E0D2859</AccessLicenseNumber>
					<UserId>marcrosoft</UserId>
					<Password>ddqpou87</Password>
			</AccessRequest>
			<?xml version=\"1.0\"?>
			<TrackRequest>
					<Request>
							<TransactionReference>
									<CustomerContext>
											<InternalKey>blah</InternalKey>
									</CustomerContext>
									<XpciVersion>1.0</XpciVersion>
							</TransactionReference>
							<RequestAction>Track</RequestAction>
					</Request>
			<TrackingNumber>$trackingNumber</TrackingNumber>
			</TrackRequest>";
	$ch = curl_init("https://www.ups.com/ups.app/xml/Track");
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_TIMEOUT, 60);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$data);
	$result=curl_exec ($ch);
	

	// echo '<!-- '. $result. ' -->';
	$data = strstr($result, '<?');
	$xml_parser = xml_parser_create();
	xml_parse_into_struct($xml_parser, $data, $vals, $index);
	xml_parser_free($xml_parser);
	$params = array();
	$level = array();
	foreach ($vals as $xml_elem) {
	 if ($xml_elem['type'] == 'open') {
	if (array_key_exists('attributes',$xml_elem)) {
			 list($level[$xml_elem['level']],$extra) = array_values($xml_elem['attributes']);
	} else {
			 $level[$xml_elem['level']] = $xml_elem['tag'];
	}
	 }
	 if ($xml_elem['type'] == 'complete') {
	$start_level = 1;
	$php_stmt = '$params';
	while($start_level < $xml_elem['level']) {
			 $php_stmt .= '[$level['.$start_level.']]';
			 $start_level++;
	}
	$php_stmt .= '[$xml_elem[\'tag\']] = $xml_elem[\'value\'];';
	eval($php_stmt);
	 }
	}
	curl_close($ch);
	return $params;
}



//$db = new redDB ("intra.elitegny.com", "root", "Elitegny11!", "EGNYMasterDB");
$db = new redDB("nintra.elitegny.com", "elitegny", "elitegny", "egnymasterdb");


//if ($_POST['af0'] == 'xx1') {
if (preg_match('/^[a-z\d_]{4,80}$/i', $_GET['trackingNumber'])) {
	
	$mode = "";
	$cleanTrackingNumber = trim( $_GET['trackingNumber'] );
	if ( substr($cleanTrackingNumber, 0, 2 ) == "1Z" ){
		$mode = "UPS";
		$someArray = upsTrack("$cleanTrackingNumber");	

		echo $mode.": ";
		
		updateUpsParseArr ( $someArray );
	}
	else{
		$mode = "FEDEX";
		$someArray = fedexTrack("$cleanTrackingNumber");	

		echo $mode.": ";			
		
		updateFedexParseArr ( $someArray );			
	}
			
			
	
	
	
		
	
	
	
} else {
	echo 'Invalid tracking number... sigh...';
}
//}

function updateFedexParseArr ( $someArray ){
	
	global $cleanTrackingNumber;
	global $db;
	print $cleanTrackingNumber;

	
	$sonum = substr($someArray [details][pono], 0, -3);
	$ship_status =  $someArray [details][status];
	$scan_status = "";
	/*
	echo '<pre>'; 
	print_r($someArray); 
	echo '</pre>';		
	*/
	if ( $ship_status != "" ){
	
		$scan_status = "Scanned";
		print $sonum." => ".$scan_status." : ".$ship_status;
	
		$sql = "update TBL_SalesOrder_Ds set trackno_status = '$ship_status<br />$sonum' where trackno = '$cleanTrackingNumber';";
				echo $sql;
	} else{
	
		echo "Not Found Info";
		$sql = "update TBL_SalesOrder_Ds set trackno_status = 'No status?' where trackno = '$cleanTrackingNumber';";
	}
	$db->update($sql);	
}

function updateUpsParseArr ( $someArray ){
		
		ECHO "<pre>";
		print_R($someArray);
		echo "</pre>";		
		
	global $cleanTrackingNumber;
	global $db;
	$arr_packinfo = $someArray [TRACKRESPONSE][SHIPMENT][PACKAGE]; 
	
	$itemsku	  = $someArray [TRACKRESPONSE][SHIPMENT][REFERENCENUMBER][VALUE]; //  prod	
	$sid	 	  = $someArray [TRACKRESPONSE][SHIPMENT][PACKAGE][REFERENCENUMBER][VALUE];  // orderid?
	$trackno	  = trim( $someArray [TRACKRESPONSE][SHIPMENT][PACKAGE][TRACKINGNUMBER] );
	$status   	  = $someArray [TRACKRESPONSE][SHIPMENT][PACKAGE][ACTIVITY][STATUS][STATUSTYPE][DESCRIPTION];	//foreach (  )
	
	if ( $cleanTrackingNumber == $trackno ){

		$sql = "select id, orderno, trackno, trackno_status, shipcity, shipaddr1 from TBL_SalesOrder_Ds where id = trackno = '$trackno' order by id desc limit 1;";
		$arr_data = $db->get_query_data($sql);
	
	/*
		ECHO "<pre>";
		print_R($someArray);
		echo "</pre>";
	*/
		if ( $status != "" ){	// picked up
		
			echo "<br />";
			if ( $sid == "" ){
				
				$sql = "update TBL_SalesOrder_Ds set trackno_status = 'In Transition: $status' where trackno = '$trackno';";
				echo $sql;
				
			}else{  // complete
				echo $sid.":".$itemsku." => ".$status;
				
				$sql = "update TBL_SalesOrder_Ds set trackno_status = '$status' where id = $sid;";
				echo $sql;				
			}			
			
			
		}
		else{
			echo "UPS didn't pick up.";
			$sql = "update TBL_SalesOrder_Ds set trackno_status = 'No status?' where id = where trackno = '$trackno';";
			echo $sql;
				
		}
		$db->update($sql);
	}
	else{
		echo "Mismatch tracking#";
		
		echo " -- $cleanTrackingNumber == $trackno";
		echo "<br />";
		$sql = "update TBL_SalesOrder_Ds set trackno_status = 'Mismatch tracking# -- $cleanTrackingNumber == $trackno' where trackno = '$cleanTrackingNumber';";
		echo $sql;		
		$db->update($sql);		
		
	}
	//}  1Z6886510342270707
	

/*
	echo '<pre>'; 
	print_r($someArray); 
	echo '</pre>';	
*/
}

?>

<html>
<head><title>Mark Sanborn's UPS Tracking PHP fucntion</title></head>
<body>
<h1>Mark Sanborn's UPS Tracking PHP Function</h1>
<h3>Please enter a valid UPS tracking number.  The XML response will be displayed as an array above</h3>
<form method="GET" action="" />
<input type="text" name="trackingNumber" />
<input type="hidden" name="af0" value="xx1" />
<input type="submit" />
</form>
</body>
</html>
