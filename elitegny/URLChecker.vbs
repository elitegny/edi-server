
'******************************************************************************************
'Script Name: mysqltest.vbs 
'Author: Martin Lydon
'Created: 26/06/2006
'Description: First attempt at MySQL Connection using local DSN - MySQL 3.51 ODBC Connector
'******************************************************************************************
'Script Initialisation
OPTION EXPLICIT

Function GetRemoteHTML(URL)

		Dim Http
		Set http = CreateObject("MSXML2.ServerXMLHTTP") 

		Http.Open "GET", URL, False
		Http.Send

		If Http.Status = 200 Then

			Dim HttpResponseStream
			Set HttpResponseStream = CreateObject("ADODB.Stream")

			HttpResponseStream.Open
			HttpResponseStream.Position = 0
			HttpResponseStream.Type = 1
			HttpResponseStream.Write Http.responseBody
			HttpResponseStream.Position = 0
			HttpResponseStream.Type = 2			
			HttpResponseStream.Charset = "euc-kr"
			
			GetRemoteHTML = HttpResponseStream.ReadText

			HttpResponseStream.Close
			Set HttpResponseStream = nothing

		Else
			GetRemoteHTML = "Error: Could not get data<br />HTTP Status: " & Http.Status

		End If

		Set Http = nothing

End Function


Dim argObj
Set argObj = Wscript.Arguments
Dim content : content = null

Dim url : url = argObj(0)



content = GetRemoteHTML(url)
If content <> "" Then
	'nwscript.echo content
	wscript.echo "done"
End If

